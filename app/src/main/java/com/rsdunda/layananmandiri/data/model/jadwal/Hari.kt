package com.rsdunda.layananmandiri.data.model.jadwal

data class Hari(
    val _id: Int,
    val dokter: String,
    val id_dokter: Int,
    val nama: String
)