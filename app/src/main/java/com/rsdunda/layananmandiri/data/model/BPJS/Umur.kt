package com.rsdunda.layananmandiri.data.model.BPJS

data class Umur(
        val umurSaatPelayanan: String,
        val umurSekarang: String
)