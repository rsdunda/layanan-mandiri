package com.rsdunda.layananmandiri.data.repository

import androidx.lifecycle.MutableLiveData
import com.rsdunda.layananmandiri.data.model.darah.Darah
import com.rsdunda.layananmandiri.data.network.CloudApi
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class InfoRepository {
    val darah: MutableLiveData<List<Darah>> = MutableLiveData()

    fun getDarah() {
        CloudApi().getDarah().enqueue(object : Callback<List<Darah>> {
            override fun onFailure(call: Call<List<Darah>>, t: Throwable) {
                darah.value = null
            }

            override fun onResponse(call: Call<List<Darah>>, response: Response<List<Darah>>) {
                if(response.isSuccessful) {
                    darah.value = response.body()
                }
            }
        })
    }
}