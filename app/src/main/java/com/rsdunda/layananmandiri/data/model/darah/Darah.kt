package com.rsdunda.layananmandiri.data.model.darah

data class Darah(
    val id_darah: Int,
    val nama_darah: String,
    val komponen: MutableList<Komponen>
)
