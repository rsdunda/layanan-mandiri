package com.rsdunda.layananmandiri.data.model.kamar

data class JENISKUNJUNGAN(
    val DESKRIPSI: String,
    val ID: String,
    val JENIS: String,
    val REF_ID: String,
    val STATUS: String
)