package com.rsdunda.layananmandiri.data.model.kamar

data class REFERENSIX(
    val JENIS_KUNJUNGAN: JENISKUNJUNGAN,
    val RUANGAN_KELAS: RUANGANKELAS
)