package com.rsdunda.layananmandiri.data.model.BPJS

data class Cob(
        val nmAsuransi: Any,
        val noAsuransi: Any,
        val tglTAT: Any,
        val tglTMT: Any
)