package com.rsdunda.layananmandiri.data.model.kamar

data class Kamar(
    val ID: String,
    val KAMAR: String,
    val KAMAR_ID: String,
    val KELAS: String,
    val NAMA: String,
    val NOPEN: String,
    val NORM: String,
    val REFERENSI: REFERENSI,
    val RESERVASI_ATAS_NAMA: Any,
    val RESERVASI_KONTAK_INFO: Any,
    val RESERVASI_NO: Any,
    val RESERVASI_TANGGAL: Any,
    val RUANGAN: String,
    val RUANGAN_ID: String,
    val STATUS: String,
    val STATUS_ID: String,
    val TEMPAT_TIDUR: String,
    val TOTAL_TAGIHAN: String
)