package com.rsdunda.layananmandiri.data.model.BPJS

data class Bpjs(
        val metadata: Metadata,
        val response: Response
)