package com.rsdunda.layananmandiri.data.model.kamar

data class KamarResponse(
    val `data`: List<Kamar>,
    val success: Boolean,
    val total: Int
)