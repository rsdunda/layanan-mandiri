package com.rsdunda.layananmandiri.data.model.Pendaftaran

data class Data(
    val _id: String,
    val NO_REGIS: String,
    val TGL_DAFTAR: String,
    val TGL_BOOKING: String,
    val NORM: String,
    val NIK: String,
    val NAMA: String,
    val TANGGAL_LAHIR: String,
    val POLI: String,
    val KEPESERTAAN: String,
    val KEPERLUAN: String,
    val JENIS_RUJUKAN: String,
    val NO_BPJS: String,
    val NO_RUJUKAN: String,
    val FOTO_KTP: String,
    val FOTO_RUJUKAN: String,
    val FOTO_KONTROL: String,
    val STATUS_BPJS: String,
    val NO_SEP: String,
    val STATUS_PELAYANAN: String,
    val PESAN: String,
    val STATUS: String,
    val session: String
)