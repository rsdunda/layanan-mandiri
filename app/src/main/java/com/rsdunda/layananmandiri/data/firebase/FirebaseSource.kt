package com.rsdunda.layananmandiri.data.firebase

import com.google.firebase.auth.FirebaseAuth


interface FirebaseSource {
    val auth: FirebaseAuth
        get() = FirebaseAuth.getInstance()
}