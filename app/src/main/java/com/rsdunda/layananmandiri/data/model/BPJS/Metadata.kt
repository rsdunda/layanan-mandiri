package com.rsdunda.layananmandiri.data.model.BPJS

data class Metadata(
        val code: String,
        val message: String,
        val requestId: String
)