package com.rsdunda.layananmandiri.data.model.darah

data class KomponenView(
    val ID: String,
    val NAMA: String,
    val STOK: String
)