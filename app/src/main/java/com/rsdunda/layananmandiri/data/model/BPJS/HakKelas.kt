package com.rsdunda.layananmandiri.data.model.BPJS

data class HakKelas(
        val keterangan: String,
        val kode: String
)