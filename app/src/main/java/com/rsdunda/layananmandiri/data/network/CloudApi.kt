package com.rsdunda.layananmandiri.data.network

import com.rsdunda.layananmandiri.data.model.BPJS.Bpjs
import com.rsdunda.layananmandiri.data.model.Pasien.Pasien
import com.rsdunda.layananmandiri.data.model.Pasien.ScanResponse
import com.rsdunda.layananmandiri.data.model.Pendaftaran.Data
import com.rsdunda.layananmandiri.data.model.Pendaftaran.Pendaftaran
import com.rsdunda.layananmandiri.data.model.Pendaftaran.PendaftaranRequest
import com.rsdunda.layananmandiri.data.model.UserRequest
import com.rsdunda.layananmandiri.data.model.Users
import com.rsdunda.layananmandiri.data.model.darah.Darah
import com.rsdunda.layananmandiri.data.model.jadwal.Jadwal
import com.rsdunda.layananmandiri.data.model.kamar.Kamar
import com.rsdunda.layananmandiri.data.model.kamar.KamarResponse
import com.rsdunda.layananmandiri.data.model.users.UsersResponse
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.*


interface CloudApi {

    @Headers("X-API-KEY: $TOKEN")
    @GET("pendaftaran/norm/{norm}")
    fun getPasien(@Path("norm") norm:String): Call<Pasien>
//
//    @GET("identitas/nik/{nik}")
//    fun checkNik(@Path("nik") nik:String): Call<Nik>
//
//    @GET("poliklinik")
//    fun getPoli(): Call<Poli>
//
    @Headers("X-API-KEY: $TOKEN")
    @GET("users/{uid}")
    fun checkUsers(@Path("uid") uid: String): Call<UsersResponse>


    @Headers("X-API-KEY: $TOKEN")
    @POST("users")
    fun simpanProfil(@Body req: UserRequest): Call<Users>
//

    @Headers("X-API-KEY: $TOKEN")
    @POST("pendaftaran")
    fun daftarkan(@Body req: PendaftaranRequest): Call<Pendaftaran>
//


    @Headers("X-API-KEY: $TOKEN")
    @GET("pendaftaran/{session}")
    fun getPendaftar(@Path("session") session:String): Call<List<Data>>
//
    @Headers("X-API-KEY: $TOKEN")
    @GET("kiosk/scan/{id}")
    fun scanQrCode(@Path("id") id:String): Call<ScanResponse>
//
    @Headers("X-API-KEY: $TOKEN")
    @GET("kamar")
    fun getKamar(): Call<List<Kamar>>
//

    @Headers("X-API-KEY: $TOKEN")
    @GET("bpjs/{nokartu}")
    fun checkBPJS(@Path("nokartu") nokartu: String): Call<Bpjs>
//

    @Headers("X-API-KEY: $TOKEN")
    @GET("darah")
    fun getDarah():Call<List<Darah>>
//
    @Headers("X-API-KEY: $TOKEN")
    @GET("jadwal")
    fun getJadwal(): Call<List<Jadwal>>


    companion object {
        const val TOKEN = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0b2tlbiI6IjdxdnQ2dDI3MzgiLCJpYXQiOjE2MjU0NzUxNTh9._QKIDbryeCYvUE8JOJ9e8Km11zZB2nNzw1N-BnXdKa8"
        val BASE_URL = "http://35.238.97.130/api/v1/"
//                val BASE_URL = "http://192.168.1.14:5000/api/v1/"
        operator fun invoke(): CloudApi {
            return Retrofit.Builder()
                    .addConverterFactory(GsonConverterFactory.create())
                    .baseUrl(BASE_URL)
                    .build()
                    .create(CloudApi::class.java)
        }
    }
}