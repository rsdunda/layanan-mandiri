package com.rsdunda.layananmandiri.data.network

import com.rsdunda.layananmandiri.data.model.kamar.KamarResponse
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET

interface ServiceApi {
    @GET("kamar")
    fun getKamar(): Call<KamarResponse>


    companion object {
        val BASE_URL = "http://36.95.55.23:8300/api/v1/"
        operator fun invoke(): ServiceApi {
            return Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl(BASE_URL)
                .build()
                .create(ServiceApi::class.java)
        }
    }
}