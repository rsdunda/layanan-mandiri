package com.rsdunda.layananmandiri.data.model.BPJS

data class StatusPeserta(
        val keterangan: String,
        val kode: String
)