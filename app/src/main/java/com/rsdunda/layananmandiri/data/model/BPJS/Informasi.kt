package com.rsdunda.layananmandiri.data.model.BPJS

data class Informasi(
        val dinsos: Any,
        val noSKTM: Any,
        val prolanisPRB: Any
)