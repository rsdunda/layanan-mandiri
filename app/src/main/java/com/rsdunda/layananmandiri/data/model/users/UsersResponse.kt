package com.rsdunda.layananmandiri.data.model.users

data class UsersResponse(
    val `data`: Data,
    val status: Int,
    val success: Boolean
)