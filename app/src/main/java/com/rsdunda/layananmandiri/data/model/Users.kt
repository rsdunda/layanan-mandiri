package com.rsdunda.layananmandiri.data.model

data class Users(
        val _id: String,
        val nik: String?,
        val norm: String?,
        val nobpjs: String?,
        val nama_lengkap: String?,
        val kelamin: String?,
        val tempat_lahir: String?,
        val tanggal_lahir: String?,
        val alamat: String?,
        val rt: String?,
        val rw: String?,
        val kelurahan: String?,
        val kecamatan: String?,
        val foto: String?,
        val role: String?,
        val session: String?,
        val status: String?
)
