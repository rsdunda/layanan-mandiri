package com.rsdunda.layananmandiri.data.model.BPJS

data class JenisPeserta(
        val keterangan: String,
        val kode: String
)