package com.rsdunda.layananmandiri.data.model.BPJS

data class Peserta(
        val cob: Cob,
        val hakKelas: HakKelas,
        val informasi: Informasi,
        val jenisPeserta: JenisPeserta,
        val mr: Mr,
        val nama: String,
        val nik: String,
        val noKartu: String,
        val norm: Int,
        val pisa: String,
        val provUmum: ProvUmum,
        val sex: String,
        val statusPeserta: StatusPeserta,
        val tglCetakKartu: String,
        val tglLahir: String,
        val tglTAT: String,
        val tglTMT: String,
        val umur: Umur
)