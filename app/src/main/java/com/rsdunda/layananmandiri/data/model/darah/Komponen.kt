package com.rsdunda.layananmandiri.data.model.darah



data class Komponen(
        val id_jenisdarah: Int,
        val nama_komponen: String,
        val stok: Int
)