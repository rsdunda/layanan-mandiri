package com.rsdunda.layananmandiri.data.model.kamar

data class RUANGANKELAS(
    val ID: String,
    val KELAS: String,
    val RUANGAN: String,
    val STATUS: String,
    val TANGGAL: String
)