package com.rsdunda.layananmandiri.data.model.Pasien

data class ScanResponse(
    val status: Int,
    val success: Boolean,
    val message: String
)