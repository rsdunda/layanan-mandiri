package com.rsdunda.layananmandiri.data.model.darah

data class DarahView(
    var ID_DARAH: String = "",
    var NAMA_DARAH: String = "",
    var KOMPONEN: MutableList<Komponen> = mutableListOf()
)

