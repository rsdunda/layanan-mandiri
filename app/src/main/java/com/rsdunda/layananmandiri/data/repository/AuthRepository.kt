package com.rsdunda.layananmandiri.data.repository

import android.app.Activity
import android.content.Context
import android.telephony.PhoneNumberUtils
import android.view.View
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.google.firebase.FirebaseException
import com.google.firebase.auth.PhoneAuthCredential
import com.google.firebase.auth.PhoneAuthOptions
import com.google.firebase.auth.PhoneAuthProvider
import com.rsdunda.layananmandiri.data.firebase.FirebaseSource
import com.rsdunda.layananmandiri.data.model.BPJS.Bpjs
import com.rsdunda.layananmandiri.data.model.BPJS.Peserta
import com.rsdunda.layananmandiri.data.model.UserRequest
import com.rsdunda.layananmandiri.data.model.Users
import com.rsdunda.layananmandiri.data.model.users.UsersResponse
import com.rsdunda.layananmandiri.data.network.CloudApi
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.concurrent.TimeUnit

class AuthRepository {

    val users: MutableLiveData<UsersResponse?> = MutableLiveData()
    val bpjs: MutableLiveData<Bpjs> = MutableLiveData()
    val regis: MutableLiveData<Boolean> = MutableLiveData()

    fun checkUser(uid: String) {
        CloudApi().checkUsers(uid).enqueue(object : Callback<UsersResponse> {
            override fun onFailure(call: Call<UsersResponse>, t: Throwable) {
                users.postValue(null)
            }

            override fun onResponse(call: Call<UsersResponse>, response: Response<UsersResponse>) {
                if(response.isSuccessful) {
                    if(response.body()?.success != false) {
                        users.value = response.body()
                    } else {
                        users.postValue(null)
                    }
                }
            }
        })
    }

    fun checkBpjs(nokartu: String) {
        CloudApi().checkBPJS(nokartu).enqueue(object : Callback<Bpjs> {
            override fun onFailure(call: Call<Bpjs>, t: Throwable) {
                bpjs.value = null
                println("DATA=== ${t.message}")
            }

            override fun onResponse(call: Call<Bpjs>, response: Response<Bpjs>) {
                println("DATA=== ${response.body()}")
                if (response.isSuccessful) {
                    bpjs.value = response.body()
                    println("DATA=== ${response.body()}")
                }
            }
        })
    }


    fun register(userRequest: UserRequest) {
        CloudApi().simpanProfil(userRequest).enqueue(object : Callback<Users> {
            override fun onFailure(call: Call<Users>, t: Throwable) {
                regis.value = false
            }

            override fun onResponse(call: Call<Users>, response: Response<Users>) {
                if (response.isSuccessful) {
                    regis.value = true
                }
            }
        })
    }

}

