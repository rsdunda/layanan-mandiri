package com.rsdunda.layananmandiri.data.model.jadwal

data class Jadwal(
    val _id: String,
    val hari: MutableList<Hari>,
    val id_poli: Int,
    val nama_poli: String
)