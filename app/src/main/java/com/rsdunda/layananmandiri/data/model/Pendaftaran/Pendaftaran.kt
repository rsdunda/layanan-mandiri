package com.rsdunda.layananmandiri.data.model.Pendaftaran

data class Pendaftaran(
    var success:Boolean,
    var status: Int,
    var message: Int,
    var `data`: Data
)