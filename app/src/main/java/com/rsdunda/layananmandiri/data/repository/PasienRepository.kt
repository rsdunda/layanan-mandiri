package com.rsdunda.layananmandiri.data.repository

import androidx.lifecycle.MutableLiveData
import com.rsdunda.layananmandiri.data.model.Pasien.Data
import com.rsdunda.layananmandiri.data.model.Pasien.Pasien
import com.rsdunda.layananmandiri.data.network.CloudApi
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class PasienRepository {

    val pasien:MutableLiveData<Data> = MutableLiveData()

    fun checkNORM(norm:String) {
        CloudApi().getPasien(norm).enqueue(object : Callback<Pasien> {
            override fun onFailure(call: Call<Pasien>, t: Throwable) {
                pasien.value = null
            }

            override fun onResponse(call: Call<Pasien>, response: Response<Pasien>) {
                if(response.isSuccessful) {
                    if(response.body()?.data != null) {
                        pasien.value = response.body()?.data
                    } else {
                        pasien.value = null
                    }
                }
            }
        })
    }


}