package com.rsdunda.layananmandiri.data.model.Pendaftaran

data class PendaftaranRequest(
        var TGL_BOOKING:String,
        var NORM:String,
        var NIK:String,
        var NAMA:String,
        var TANGGAL_LAHIR:String,
        var POLI:String,
        var KEPESERTAAN:String,
        var KEPERLUAN:String,
        var JENIS_RUJUKAN:String,
        var NO_BPJS:String,
        var NO_RUJUKAN:String,
        var FOTO_KTP:String,
        var FOTO_RUJUKAN:String,
        var FOTO_RUJUKAN_LAMA:String,
        var FOTO_KONTROL:String,
        var STATUS_BPJS:String,
        var NO_SEP:String,
        var STATUS_PELAYANAN:String = "0",
        var STATUS:String = "0",
        var session:String
)