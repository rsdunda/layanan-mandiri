package com.rsdunda.layananmandiri.data.model.users

data class Data(
    val __v: Int,
    val _id: String,
    val alamat: String,
    val foto: String,
    val kecamatan: String,
    val kelamin: String,
    val kelurahan: String,
    val nama_lengkap: String,
    val nik: String,
    val nobpjs: String,
    val norm: String,
    val role: String,
    val rt: String,
    val rw: String,
    val session: String,
    val status: String,
    val tanggal_lahir: String,
    val tempat_lahir: String
)