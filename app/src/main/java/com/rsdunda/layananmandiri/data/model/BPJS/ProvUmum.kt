package com.rsdunda.layananmandiri.data.model.BPJS

data class ProvUmum(
        val kdProvider: String,
        val nmProvider: String
)