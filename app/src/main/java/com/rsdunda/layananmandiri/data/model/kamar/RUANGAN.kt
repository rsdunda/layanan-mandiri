package com.rsdunda.layananmandiri.data.model.kamar

data class RUANGAN(
    val DESKRIPSI: String,
    val ID: String,
    val JENIS: String,
    val JENIS_KUNJUNGAN: String,
    val REFERENSI: REFERENSIX,
    val REF_ID: String,
    val STATUS: String
)