package com.rsdunda.layananmandiri.ui.fragments.FormProfile

import android.app.ProgressDialog
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.asLiveData
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.observe
import androidx.navigation.fragment.findNavController
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.rsdunda.layananmandiri.MainActivity
import com.rsdunda.layananmandiri.R
import com.rsdunda.layananmandiri.data.model.UserRequest
import com.rsdunda.layananmandiri.databinding.FragmentBpjsDialogBinding
import com.rsdunda.layananmandiri.ui.auth.AuthViewModel
import com.rsdunda.layananmandiri.util.SharedUsers
import com.rsdunda.layananmandiri.util.UserManager
import kotlinx.coroutines.launch

class BpjsDialogFragment : BottomSheetDialogFragment() {
    private var _binding: FragmentBpjsDialogBinding? = null
    private val binding get() = _binding
    private val authViewModel: AuthViewModel by activityViewModels()
    private lateinit var sharedUsers: SharedUsers
    private lateinit var progressLoading: ProgressDialog
//    private lateinit var sharedUsers: SharedUsers
    private lateinit var userManager: UserManager
    var session = ""
    override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentBpjsDialogBinding.inflate(inflater, container, false)
        userManager = UserManager(requireContext())
        sharedUsers = SharedUsers(requireContext())
        userManager.session.asLiveData().observe(viewLifecycleOwner) {
            session = it
        }
        progressLoading = ProgressDialog(context)
        progressLoading.window?.setBackgroundDrawableResource(android.R.color.transparent)


        binding?.btnBPJS?.setOnClickListener {
            findNavController().navigate(R.id.action_bpjsDialogFragment_to_bpjsFragment)
        }

        binding?.btnUMUM?.setOnClickListener {
            progressLoading.show()
            progressLoading.setContentView(R.layout.dialog_loading)
            authViewModel.let {
                val user = UserRequest(
                    nik = it.nik.value.toString(),
                    norm = "",
                    nobpjs = "",
                    nama_lengkap = it.nama_lengkap.value.toString(),
                    kelamin = it.kelamin.value.toString(),
                    tempat_lahir = it.tempat_lahir.value.toString(),
                    tanggal_lahir = it.tanggal_lahir.value.toString(),
                    alamat =  it.alamat.value.toString(),
                    rt = it.rt.value.toString(),
                    rw = it.rw.value.toString(),
                    kelurahan = if(it.kelurahan.value.isNullOrEmpty()) "" else it.kelurahan.value.toString(),
                    kecamatan = if(it.kecamatan.value.isNullOrEmpty()) "" else it.kecamatan.value.toString(),
                    foto = if(it.foto.value.isNullOrEmpty()) "" else it.foto.value.toString(),
                    session = session
                )
                it.registerUser(user)

            }

            authViewModel.regisInfo.observe(viewLifecycleOwner, Observer {
                if(it !== null && it == true) {
                    progressLoading.dismiss()
                    lifecycleScope.launch {
                        authViewModel.let {
                            userManager.storeUsers(
                                no_hp = it.number.value.toString(),
                                nama = it.nama_lengkap.value.toString(),
                                norm = it.norm.value.toString(),
                                nobpjs = it.nobpjs.value.toString(),
                                alamat = it.alamat.value.toString(),
                                session = session,
                                auth = true,
                                foto = it.foto.value.toString(),
                                role = it.role.value.toString(),
                                nik = it.nik.value.toString(),
                                kelamin = it.kelamin.value.toString()
                            )
//                            sharedUsers.saveUsers(
//                                uid = it.session.value.toString(),
//                                nomor_hp = it.number.value.toString(),
//                                nama_lengkap = it.nama_lengkap.value.toString(),
//                                norm = it.norm.value.toString(),
//                                nobpjs = it.nobpjs.value.toString(),
//                                alamat = it.alamat.value.toString(),
//                                session = it.session.value.toString(),
//                                isAuth = true,
//                                foto = it.foto.value.toString(),
//                                role = it.role.value.toString(),
//                                nik = it.nik.value.toString(),
//                                kelamin = it.kelamin.value.toString()
//                            )
                        }
                    }
                    sharedUsers.isAuth = true
                    activity?.finish()
                    startActivity(Intent(requireContext(), MainActivity::class.java))
                }

                if(it !== null && it == false) {
                    progressLoading.dismiss()
                    Toast.makeText(requireContext(), "Gagal simpan profil", Toast.LENGTH_SHORT).show()
                }
            })
        }

        return binding?.root
    }


    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }

}