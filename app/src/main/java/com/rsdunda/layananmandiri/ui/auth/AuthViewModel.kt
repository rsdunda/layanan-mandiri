package com.rsdunda.layananmandiri.ui.auth

import android.app.Application
import androidx.lifecycle.*
import com.rsdunda.layananmandiri.data.model.BPJS.Bpjs
import com.rsdunda.layananmandiri.data.model.UserRequest
import com.rsdunda.layananmandiri.data.model.Users
import com.rsdunda.layananmandiri.data.model.users.UsersResponse
import com.rsdunda.layananmandiri.data.repository.AuthRepository
import com.rsdunda.layananmandiri.util.SharedUsers
import kotlinx.coroutines.launch


class AuthViewModel(application: Application) : AndroidViewModel(application) {
    private val authRepo = AuthRepository()
    private var _number = MutableLiveData<String>()
    private var _verifyId = MutableLiveData<String>()
    private var _isAuth = MutableLiveData<Boolean>()

    private var _nik = MutableLiveData<String>()
    private var _norm = MutableLiveData<String>()
    private var _nobpjs = MutableLiveData<String>()
    private var _nama_lengkap = MutableLiveData<String>()
    private var _kelamin = MutableLiveData<String>()
    private var _tempat_lahir = MutableLiveData<String>()
    private var _tanggal_lahir = MutableLiveData<String>()
    private var _alamat = MutableLiveData<String>()
    private var _rt = MutableLiveData<String>()
    private var _rw = MutableLiveData<String>()
    private var _kelurahan = MutableLiveData<String>()
    private var _kecamatan = MutableLiveData<String>()
    private var _foto = MutableLiveData<String>()
    private var _role = MutableLiveData<String>()
    private var _session = MutableLiveData<String>()
    private var _status = MutableLiveData<String>()


    val verifyId: LiveData<String> = _verifyId
    val number: LiveData<String> = _number
    val users: LiveData<UsersResponse?> = authRepo.users
    val dataBpjs: LiveData<Bpjs> = authRepo.bpjs
    val regisInfo: LiveData<Boolean> = authRepo.regis


    val nik: LiveData<String> = _nik
    val norm: LiveData<String> = _norm
    val nobpjs: LiveData<String> = _nobpjs
    val nama_lengkap: LiveData<String> = _nama_lengkap
    val kelamin: LiveData<String> = _kelamin
    val tempat_lahir: LiveData<String> = _tempat_lahir
    val tanggal_lahir: LiveData<String> = _tanggal_lahir
    val alamat: LiveData<String> = _alamat
    val rt: LiveData<String> = _rt
    val rw: LiveData<String> = _rw
    val kelurahan: LiveData<String> = _kelurahan
    val kecamatan: LiveData<String> = _kecamatan
    val foto: LiveData<String> = _foto
    val role: LiveData<String> = _role


    fun setVerifyId(id: String) {
        _verifyId.value = id
    }

    fun setNumber(number: String) {
        _number.value = number
    }

    fun setNik(nik: String) {
        _nik.value = nik
    }

    fun setNorm(norm: String) {
        _norm.value = norm
    }

    fun setNobpjs(nobpjs: String) {
        _nobpjs.value = nobpjs
    }

    fun setNamaLengkap(nama_lengkap: String) {
        _nama_lengkap.value = nama_lengkap
    }

    fun setKelamin(kelamin: String) {
        _kelamin.value = kelamin
    }

    fun setTempatLahir(tempat_lahir: String) {
        _tempat_lahir.value = tempat_lahir
    }

    fun setTanggalLahir(tanggal_lahir: String) {
        _tanggal_lahir.value = tanggal_lahir
    }

    fun setAlamat(alamat: String) {
        _alamat.value = alamat
    }

    fun setRt(rt: String) {
        _rt.value = rt
    }

    fun setRw(rw: String) {
        _rw.value = rw
    }

    fun setKelurahan(kelurahan: String) {
        _kelurahan.value = kelurahan
    }

    fun setKecamatan(kecamatan: String) {
        _kecamatan.value = kecamatan
    }

    fun setFoto(foto: String) {
        _foto.value = foto
    }

    fun setRole(role: String) {
        _role.value = role
    }

    fun setSession(session: String) {
        _session.value = session
    }

    fun setStatus(status: String) {
        _status.value = status
    }

    fun setLogin(login: Boolean) {
        _isAuth.value = login
    }


    fun checkUsers(uid: String) {
        viewModelScope.launch {
            authRepo.checkUser(uid)
        }
    }

    fun checkBPJS(nokartu: String) {
        viewModelScope.launch {
            authRepo.checkBpjs(nokartu)
        }
    }

    fun resetDataBpjs() {
        viewModelScope.launch {
            authRepo.bpjs.value = null
        }
    }


    fun registerUser(userRequest: UserRequest) {
        viewModelScope.launch {
            authRepo.register(userRequest)
        }
    }


}