package com.rsdunda.layananmandiri.ui.fragments.Auth

import android.app.ProgressDialog
import android.os.Bundle
import android.telephony.PhoneNumberUtils
import android.text.Editable
import android.text.TextWatcher
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import com.google.android.material.button.MaterialButton
import com.google.firebase.FirebaseException
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.PhoneAuthCredential
import com.google.firebase.auth.PhoneAuthOptions
import com.google.firebase.auth.PhoneAuthProvider
import com.rsdunda.layananmandiri.R
import com.rsdunda.layananmandiri.databinding.FragmentPhoneBinding
import com.rsdunda.layananmandiri.ui.auth.AuthViewModel
import com.rsdunda.layananmandiri.util.SharedUsers
import com.rsdunda.layananmandiri.util.UserManager
import java.util.concurrent.TimeUnit

@Suppress("DEPRECATION")
class PhoneFragment : Fragment() {
    lateinit var nomor_hp: EditText
    lateinit var btn_next: MaterialButton
    private lateinit var auth: FirebaseAuth
    private lateinit var progressLoading: ProgressDialog
    private var _binding: FragmentPhoneBinding? = null
    private val binding get() = _binding
//    private lateinit var sharedUsers: SharedUsers
//    private lateinit var userManager: UserManager
    private val authViewModel: AuthViewModel by activityViewModels()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
//        val view = inflater.inflate(R.layout.fragment_phone, container, false)
        _binding = FragmentPhoneBinding.inflate(inflater, container, false)
//        sharedUsers = SharedUsers(requireContext())
//        userManager = UserManager(requireContext())

        auth = FirebaseAuth.getInstance()
        nomor_hp = binding!!.nomorHp
        btn_next = binding!!.btnNext
        btn_next.isEnabled = false


        progressLoading = ProgressDialog(context)
        progressLoading.window?.setBackgroundDrawableResource(android.R.color.transparent)



        nomor_hp.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                if (s.toString().length == 1 && s.toString().startsWith("0")) {
                    s?.clear()
                }

                btn_next.isEnabled = s.toString().length > 0

            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

            }
        })

        btn_next.setOnClickListener {
            btn_next.isEnabled = false
            kirimOTP()
        }

        return binding?.root

    }


    private fun kirimOTP() {
        progressLoading.show()
        progressLoading.setContentView(R.layout.dialog_loading)
//        val phoneNumberInE164 = PhoneNumberUtils.formatNumberToE164("+62${nomor_hp.text.trim()}", "ID")
//        println("+62${nomor_hp.text.trim()}")
        val options = activity?.let {
            PhoneAuthOptions.newBuilder(auth)
                    .setPhoneNumber("+62${nomor_hp.text.trim()}")
                    .setTimeout(60L, TimeUnit.SECONDS)
                    .setActivity(it)
                    .setCallbacks(object : PhoneAuthProvider.OnVerificationStateChangedCallbacks() {
                        override fun onCodeSent(p0: String, p1: PhoneAuthProvider.ForceResendingToken) {
                            super.onCodeSent(p0, p1)
                            progressLoading.setContentView(R.layout.dialog_success)
                            authViewModel.setVerifyId(p0)
                            authViewModel.setNumber(nomor_hp.text.toString())
                            findNavController().navigate(R.id.action_phoneFragment_to_verifyFragment)
                            btn_next.isEnabled = false
                            nomor_hp.text.clear()
                            progressLoading.dismiss()
                        }

                        override fun onCodeAutoRetrievalTimeOut(p0: String) {
                            super.onCodeAutoRetrievalTimeOut(p0)
                        }

                        override fun onVerificationCompleted(p0: PhoneAuthCredential) {
                            progressLoading.setContentView(R.layout.dialog_success)
                            progressLoading.dismiss()
                        }

                        override fun onVerificationFailed(p0: FirebaseException) {}
                    }).build()
        }
        PhoneAuthProvider.verifyPhoneNumber(options!!)
    }


    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

}


