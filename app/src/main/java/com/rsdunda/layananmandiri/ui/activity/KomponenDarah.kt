package com.rsdunda.layananmandiri.ui.activity

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.rsdunda.layananmandiri.R
import com.rsdunda.layananmandiri.data.model.darah.Komponen
import com.rsdunda.layananmandiri.ui.adapter.KomponenDarahAdapter
import com.rsdunda.layananmandiri.ui.viewmodel.DarahViewModel
import org.json.JSONArray

class KomponenDarah : AppCompatActivity() {
    lateinit var rv_komponen: RecyclerView
    lateinit var komponenDarahAdapter: KomponenDarahAdapter
    private lateinit var darahViewModel: DarahViewModel
    private var komponentList:MutableList<Komponen> = ArrayList()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_komponen_darah)
        val extras = intent.extras
        supportActionBar?.title = "Komponen Darah ${extras?.getString("nama_darah")}"
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setHomeButtonEnabled(true)
        actionBar?.title = "Komponen Darah ${extras?.getString("nama_darah")}"


        darahViewModel = ViewModelProvider(this).get(DarahViewModel::class.java)
        komponenDarahAdapter = KomponenDarahAdapter(mutableListOf(), this, darahViewModel)
        rv_komponen = findViewById(R.id.rv_komponen)
        rv_komponen.layoutManager = LinearLayoutManager(this)
        val sharedPreferences = getSharedPreferences("shared preferences", MODE_PRIVATE)
//        val gson = Gson()
        val json = sharedPreferences.getString("komponen", null)

        val jsonArr = JSONArray(json)
        for (i in 0 until jsonArr.length()) {
            val jsonData = jsonArr.getJSONObject(i)
            val idJenis = jsonData.getString("id_jenisdarah")
            val namaKomponen = jsonData.getString("nama_komponen")
            val stok = jsonData.getString("stok")
            val kom = Komponen(idJenis.toInt(), namaKomponen, stok.toInt())
            komponentList.add(kom)
        }

        komponenDarahAdapter.updateList(komponentList)

        rv_komponen.adapter = komponenDarahAdapter



    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        super.onOptionsItemSelected(item)
        when(item.itemId) {
            android.R.id.home -> {
                onBackPressed()
            }
        }
        return true
    }

    override fun onBackPressed() {
        super.onBackPressed()
        finish()
        val intent = Intent(this, KantongDarah::class.java)
        startActivity(intent)
    }


}