package com.rsdunda.layananmandiri.ui.viewmodel.fragments.Home

import android.annotation.SuppressLint
import android.app.Dialog
import android.content.Intent
import android.graphics.drawable.InsetDrawable
import android.net.Uri
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.navigation.fragment.findNavController
import com.denzcoskun.imageslider.ImageSlider
import com.denzcoskun.imageslider.constants.ScaleTypes
import com.denzcoskun.imageslider.models.SlideModel
import com.rsdunda.layananmandiri.R
import com.rsdunda.layananmandiri.databinding.FragmentHomeBinding
import com.rsdunda.layananmandiri.ui.activity.InfoBed
import com.rsdunda.layananmandiri.ui.activity.JadwalDokter
import com.rsdunda.layananmandiri.ui.activity.KantongDarah
import com.rsdunda.layananmandiri.ui.fragments.Pendaftaran.Pendaftaran
import com.rsdunda.layananmandiri.ui.tentang.Tentang
import com.rsdunda.layananmandiri.util.SharedUsers

class HomeFragment : Fragment() {
    private var _binding: FragmentHomeBinding? = null
    private val binding get() = _binding
    private lateinit var sharedUsers: SharedUsers
    @SuppressLint("SetTextI18n")
    override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        _binding = FragmentHomeBinding.inflate(inflater, container, false)
        sharedUsers = SharedUsers(requireContext())





        binding?.textNama?.text = sharedUsers.nama_lengkap
        binding?.textNik?.text = "NIK ${sharedUsers.nik}"

        binding?.btnPendaftaran?.setOnClickListener {
            findNavController().navigate(R.id.action_homeFragment_to_pendaftaranDialogFragment)
//            activity?.finish()
//            startActivity(Intent(requireContext(), Pendaftaran::class.java))
        }

        binding?.tombolCekBpjs?.setOnClickListener {
            findNavController().navigate(R.id.action_homeFragment_to_checkBpjs)
        }

        binding?.tombolCekRm?.setOnClickListener {
            val dialog = Dialog(requireContext())
            dialog.setContentView(R.layout.modal_rekamedik)
            dialog.window?.setBackgroundDrawable(InsetDrawable(activity?.getDrawable(R.drawable.background_modal), 20))
            dialog.window?.setLayout(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT
            )
            dialog.setCancelable(true)
            dialog.show()

            val btn_1 = dialog.findViewById<TextView>(R.id.btn_no_1)
            val btn_2 = dialog.findViewById<TextView>(R.id.btn_no_2)
            val btn_3 = dialog.findViewById<TextView>(R.id.btn_no_3)

            btn_1.setOnClickListener { openWhatsapp("6281355610549") }
            btn_2.setOnClickListener { openWhatsapp("6285298205196") }
            btn_3.setOnClickListener { openWhatsapp("6282293723494") }
        }

        binding?.tombolInfoDarah?.setOnClickListener {
            activity?.finish()
            requireContext().startActivity(Intent(requireContext(), KantongDarah::class.java))
        }

        binding?.tombolInfoJadwalDokter?.setOnClickListener {
            activity?.finish()
            requireContext().startActivity((Intent(requireContext(), JadwalDokter::class.java)))
        }

        binding?.tombolInfoBed?.setOnClickListener {
            activity?.finish()
            requireContext().startActivity((Intent(requireContext(), InfoBed::class.java)))
        }


        val imageList = ArrayList<SlideModel>()
        imageList.add(SlideModel("http://www.rsdunda.com/wp-content/uploads/2020/06/IMG-20200616-WA0015.jpg", "Bupati APEL PAGI BERSAMA JAJARAN RS DUNDA LIMBOTO"))
        imageList.add(SlideModel("http://www.rsdunda.com/wp-content/uploads/2020/04/WhatsApp-Image-2020-03-24-at-10.14.401.jpeg", "BUPATI TINJAU KESIAPAN RS DUNDA HADAPI COVID-19"))
        imageList.add(SlideModel("http://www.rsdunda.com/wp-content/uploads/2020/04/WhatsApp-Image-2020-01-29-at-09.40.171.jpeg", "RS DUNDA LIMBOTO GENCAR EDUKASI WARGA TENTANG COVID-19"))
        imageList.add(SlideModel("http://www.rsdunda.com/wp-content/uploads/2020/04/DSC2447.jpg", "WAKIL KETUA DPR RI RACHMAT GOBEL BANTU APD UNTUK RS DUNDA"))
        imageList.add(SlideModel("http://www.rsdunda.com/wp-content/uploads/2020/07/WhatsApp-Image-2020-07-09-at-08.38.37.jpeg", "Dr.TRISON BIALANGI JABAT PELAKSANA TUGAS DIREKTUR RS DUNDA LIMBOTO"))


        val imageSlider = binding?.root?.findViewById<ImageSlider>(R.id.image_slider)
        imageSlider?.setImageList(imageList, ScaleTypes.CENTER_CROP)

        binding?.btnTentang?.setOnClickListener {
            context?.startActivity(Intent(requireContext(), Tentang::class.java))
        }

        return binding?.root
    }


    private fun openWhatsapp(nomor:String) {
        val intent = Intent(Intent.ACTION_VIEW)
        intent.setData(Uri.parse("https://wa.me/$nomor/?text=" + ""))
        startActivity(intent)
    }

    override fun onDestroy() {
        _binding = null
        super.onDestroy()
    }
}