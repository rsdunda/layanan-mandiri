package com.rsdunda.layananmandiri.ui.auth

import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.navigation.findNavController
import androidx.navigation.ui.setupActionBarWithNavController
import androidx.navigation.ui.setupWithNavController
import com.rsdunda.layananmandiri.R
import com.rsdunda.layananmandiri.databinding.ActivityAuthBinding

class AuthActivity : AppCompatActivity() {
    private lateinit var binding: ActivityAuthBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityAuthBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)
        actionBar?.hide()
        supportActionBar?.hide()
//        actionBar?.setBackgroundDrawable(ColorDrawable(Color.WHITE))
//        supportActionBar?.setBackgroundDrawable(ColorDrawable(Color.WHITE))
//        actionBar?.elevation = 0F
//        supportActionBar?.elevation = 0F
//        actionBar?.setHomeAsUpIndicator(R.drawable.ic_home)
//        supportActionBar?.setHomeAsUpIndicator(R.drawable.ic_home)

//        setupActionBarWithNavController(findNavController(R.id.authfragment))
    }

//    override fun onSupportNavigateUp(): Boolean {
//        val navController = findNavController(R.id.authfragment)
//        return navController.navigateUp() || super.onSupportNavigateUp()
//    }
}