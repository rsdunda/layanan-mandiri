package com.rsdunda.layananmandiri.ui.activity

import android.app.ProgressDialog
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import android.widget.ProgressBar
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.rsdunda.layananmandiri.MainActivity
import com.rsdunda.layananmandiri.R
import com.rsdunda.layananmandiri.ui.adapter.DarahAdapter
import com.rsdunda.layananmandiri.ui.viewmodel.DarahViewModel
import com.rsdunda.layananmandiri.util.toast

class KantongDarah : AppCompatActivity() {
    lateinit var rv_darah: RecyclerView
    lateinit var swipeRefreshLayout: SwipeRefreshLayout
    lateinit var darahAdapter: DarahAdapter
    private lateinit var darahViewModel: DarahViewModel
    private lateinit var loading: ProgressBar

    //    private lateinit var dialog: Dialog
    private lateinit var progressLoading: ProgressDialog
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_kantong_darah)
        supportActionBar?.title = "Informasi Kantong Darah"
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setHomeButtonEnabled(true)
        actionBar?.title = "Informasi Kantong Darah"

        darahViewModel = ViewModelProvider(this).get(DarahViewModel::class.java)
        darahAdapter = DarahAdapter(mutableListOf(), this, darahViewModel)

        // component
        rv_darah = findViewById(R.id.rv_darah)
        swipeRefreshLayout = findViewById(R.id.swipeDarah)
        loading = findViewById(R.id.loading)
//        dialog = Dialog(this)
//        dialog.setContentView(R.layout.dialog_loading)
        // loading
        progressLoading = ProgressDialog(this@KantongDarah)
        progressLoading.window?.setBackgroundDrawableResource(android.R.color.transparent)



        rv_darah.apply {
            layoutManager = GridLayoutManager(this@KantongDarah, 3)
            adapter = darahAdapter
        }

        swipeRefreshLayout.setOnRefreshListener {
            loading.visibility = View.VISIBLE
            initLoad()
        }

        initLoad()
    }

    private fun initLoad() {
//        dialog.show()
        darahViewModel.getDarah()
        darahViewModel.isLoading.observe(this, Observer {
            if (it) {
                progressLoading.show()
                progressLoading.setContentView(R.layout.dialog_loading)
            } else {
                progressLoading.dismiss()
            }
        })
        darahViewModel.listenDarah().observe(this, Observer {
            if (it != null) {
                rv_darah.adapter?.let { a ->
                    if (a is DarahAdapter) {
                        a.updateList(it)
                    }
                    loading.visibility = View.GONE
                }

            } else {
                toast("Ada masalah silahkan hubungi admin")
                loading.visibility = View.GONE
            }

        })

        swipeRefreshLayout.isRefreshing = false
    }


    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        super.onOptionsItemSelected(item)
        when (item.itemId) {
            android.R.id.home -> {
                onBackPressed()
            }
        }
        return true
    }

    override fun onBackPressed() {
        super.onBackPressed()
        finish()
        val intent = Intent(this@KantongDarah, MainActivity::class.java)
        startActivity(intent)

    }
}