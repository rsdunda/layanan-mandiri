package com.rsdunda.layananmandiri.ui.viewmodel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.viewModelScope
import com.rsdunda.layananmandiri.data.model.darah.Darah
import com.rsdunda.layananmandiri.data.repository.InfoRepository
import kotlinx.coroutines.launch

class InfoViewModel(application: Application): AndroidViewModel(application) {
    private val infoRepository = InfoRepository()
    val darah: LiveData<List<Darah>> = infoRepository.darah


    fun getDarah() {
        viewModelScope.launch {
            infoRepository.getDarah()
        }
    }
}