package com.rsdunda.layananmandiri.ui.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.rsdunda.layananmandiri.R
import com.rsdunda.layananmandiri.data.model.jadwal.Hari
import com.rsdunda.layananmandiri.ui.viewmodel.JadwalViewModel

class JadwalHariAdapter(
    private var hari: MutableList<Hari>,
    private var context: Context,
    private var jadwalViewModel: JadwalViewModel
): RecyclerView.Adapter<JadwalHariAdapter.ViewHolder>() {



    class ViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {
        val text_nama_hari = itemView.findViewById<TextView>(R.id.text_nama_hari)
        val text_nama_dok = itemView.findViewById<TextView>(R.id.text_nama_dok)
        fun bind(data: Hari, context: Context, jadwalViewModel: JadwalViewModel) {
            text_nama_hari.text = data.nama
            text_nama_dok.text = data.dokter
        }
    }

    fun updateList(data: List<Hari>) {
        hari.clear()
        hari.addAll(data)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.items_hari, parent, false))
    }

    override fun getItemCount() = hari.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) = holder.bind(hari[position], context, jadwalViewModel)
}