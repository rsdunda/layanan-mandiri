package com.rsdunda.layananmandiri.ui.fragments.Pendaftaran

import android.content.DialogInterface
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.rsdunda.layananmandiri.R
import com.rsdunda.layananmandiri.databinding.FragmentKepesertaanDialogBinding
import com.rsdunda.layananmandiri.ui.viewmodel.PendaftaranViewModel

class KepesertaanDialogFragment : BottomSheetDialogFragment() {
    private var _binding: FragmentKepesertaanDialogBinding? = null
    private val binding get() = _binding
    private val pendaftaranViewModel: PendaftaranViewModel by activityViewModels()
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        _binding = FragmentKepesertaanDialogBinding.inflate(inflater, container, false)
        isCancelable = false

        binding?.btnBpjs?.setOnClickListener {
//            pendaftaranViewModel.setBPJS("1234")
            findNavController().navigate(R.id.action_kepesertaanDialogFragment_to_bpjsFragment2)
//            findNavController().navigate(R.id.action_kepesertaanDialogFragment_to_step3Fragment)

        }

        binding?.btnUmum?.setOnClickListener {
            pendaftaranViewModel.setBPJS("")
            findNavController().navigate(R.id.action_kepesertaanDialogFragment_to_step3Fragment)
        }


        return binding?.root
    }

    override fun onCancel(dialog: DialogInterface) {
        super.onCancel(dialog)
        Toast.makeText(requireContext(), "kembali", Toast.LENGTH_SHORT).show()
        findNavController().navigate(R.id.action_kepesertaanDialogFragment_to_step2Fragment)
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }

}
