package com.rsdunda.layananmandiri.ui.fragments.Pendaftaran

import android.annotation.SuppressLint
import android.app.Activity
import android.app.DatePickerDialog
import android.app.ProgressDialog
import android.content.Intent
import android.graphics.Bitmap
import android.os.Bundle
import android.provider.MediaStore
import android.text.Editable
import android.text.TextWatcher
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.DatePicker
import android.widget.Toast
import androidx.fragment.app.activityViewModels
import coil.load
import coil.transform.RoundedCornersTransformation
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.OnProgressListener
import com.google.firebase.storage.StorageReference
import com.google.firebase.storage.UploadTask
import com.rsdunda.layananmandiri.R
import com.rsdunda.layananmandiri.data.model.Pendaftaran.PendaftaranRequest
import com.rsdunda.layananmandiri.data.model.Pendaftaran.Pendaftaran
import com.rsdunda.layananmandiri.data.network.CloudApi
import com.rsdunda.layananmandiri.databinding.FragmentStep3Binding
import com.rsdunda.layananmandiri.ui.auth.AuthViewModel
import com.rsdunda.layananmandiri.ui.viewmodel.PendaftaranViewModel
import com.rsdunda.layananmandiri.util.SharedUsers
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.ByteArrayOutputStream
import java.text.SimpleDateFormat
import java.util.*


class Step3Fragment : Fragment() {
    private var _binding: FragmentStep3Binding? = null
    private val binding get() = _binding
    private lateinit var datePickerDialog: DatePickerDialog
    private lateinit var dateFormater: SimpleDateFormat
    private lateinit var sharedUsers: SharedUsers
    private val pendaftaranViewModel: PendaftaranViewModel by activityViewModels()
    private val authViewModel: AuthViewModel by activityViewModels()

    lateinit var storage:FirebaseStorage
    lateinit var storageRef: StorageReference

    private lateinit var progressLoading: ProgressDialog
    var NIK_PASIEN = ""
    var NOBPJS = ""
    var JENIS_RUJUKAN = ""
    var FOTO_RESUME = ""
    var FOTO_RUJUKAN = ""
    var FOTO_RUJUKAN_LAMA = ""
    val REQUEST_IMAGE_RUJUKAN = 2
    val REQUEST_IMAGE_RUJUKAN_LAMA = 4
    val REQUEST_IMAGE_RESUME = 3


    @SuppressLint("SetTextI18n")
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        _binding = FragmentStep3Binding.inflate(inflater, container, false)
        sharedUsers = SharedUsers(requireContext())
        storage = FirebaseStorage.getInstance()
        storageRef = storage.reference

        dateFormater = SimpleDateFormat("MM-dd-yyyy", Locale.ROOT)

        authViewModel.dataBpjs.observe(viewLifecycleOwner, androidx.lifecycle.Observer {
            if (it != null) {
                NOBPJS = it.response.peserta.noKartu
                NIK_PASIEN = it.response.peserta.nik
            }
        })

        if (pendaftaranViewModel.BPJS.value.toString() === "") {
            binding?.cardJenisRujukan?.visibility = View.GONE
            binding?.cardUploadBerkas?.visibility = View.GONE
        } else {
            binding?.cardJenisRujukan?.visibility = View.VISIBLE
            binding?.cardUploadBerkas?.visibility = View.VISIBLE
        }



        // progres dialog
        progressLoading = ProgressDialog(context)
        progressLoading.window?.setBackgroundDrawableResource(android.R.color.transparent)
        val PENDAFTAR = activity?.intent?.extras?.getString("PENDAFTAR").toString()

        if (PENDAFTAR == "SENDIRI") {
            binding?.nikPasien?.visibility = View.GONE
        }

        if (PENDAFTAR == "ORANG") {
            binding?.nikPasien?.visibility = View.VISIBLE
        }




        inputData()


        binding?.btnDaftarkan?.setOnClickListener {
            var KEPESERTAAN = ""
            if (pendaftaranViewModel.BPJS.value.toString() === "") {
                KEPESERTAAN = "UMUM"
                JENIS_RUJUKAN = ""
            } else {
                KEPESERTAAN = "BPJS"

                if(binding?.jenisRujukan?.selectedItem.toString() == "-- pilih --") {
                    binding?.errorRujukan?.setText("Pilih jenis rujukan")
                    return@setOnClickListener
                }

                if(binding?.jenisRujukan?.selectedItem.toString() == "LAMA") {
                    if(FOTO_RESUME == "") {
                        Toast.makeText(requireContext(), "Foto resume medis atau surat kontrol belum ada", Toast.LENGTH_SHORT).show()
                        return@setOnClickListener
                    }
                }

                if(binding?.jenisRujukan?.selectedItem.toString() == "BARU") {
                    if(FOTO_RUJUKAN == "" ) {
                        Toast.makeText(requireContext(), "Foto rujukan belum ada", Toast.LENGTH_SHORT).show()
                        return@setOnClickListener
                    }
                }




            }

            if (binding?.etTanggalDaftar?.text?.trim()?.isEmpty()!!) {
                binding?.errorTanggalDaftar?.setText("Tanggal belum di setting")
                return@setOnClickListener
            }

            if (binding?.etNik?.text?.trim()?.isEmpty()!!) {
                binding?.errorNik?.setText("Nik Belum disisi")
                return@setOnClickListener
            }

            if (binding?.poliTujuan?.selectedItem.toString() == "-- pilih --") {
                binding?.errorPoli?.setText("Pilih poli tujuan")
                return@setOnClickListener
            }

            if (binding?.keperluanTujuan?.selectedItem.toString() == "-- pilih --") {
                binding?.errorKeperluan?.setText("Pilih poli keperluan")
                return@setOnClickListener
            }






            val data = PendaftaranRequest(
                TGL_BOOKING = binding?.etTanggalDaftar?.text.toString(),
                NORM = pendaftaranViewModel.NORM.value.toString(),
                NIK = binding?.etNik?.text?.toString()!!,
                NAMA = pendaftaranViewModel.NAMA.value.toString(),
                TANGGAL_LAHIR = pendaftaranViewModel.TANGGAL_LAHIR.value.toString(),
                POLI = binding?.poliTujuan?.selectedItem.toString(),
                KEPESERTAAN = KEPESERTAAN,
                KEPERLUAN = binding?.keperluanTujuan?.selectedItem.toString(),
                JENIS_RUJUKAN = JENIS_RUJUKAN,
                NO_BPJS = NOBPJS,
                NO_RUJUKAN = "",
                FOTO_KTP = pendaftaranViewModel.FOTO_KTP.value.toString(),
                FOTO_RUJUKAN = FOTO_RUJUKAN,
                FOTO_RUJUKAN_LAMA = FOTO_RUJUKAN_LAMA,
                FOTO_KONTROL = FOTO_RESUME,
                STATUS_BPJS = "",
                NO_SEP = "",
                STATUS_PELAYANAN = "0",
                STATUS = "0",
                session = sharedUsers.session.toString()
            )
            if (
                binding?.etTanggalDaftar?.text?.trim()?.isNotEmpty()!! &&
                binding?.etNik?.text?.trim()?.isNotEmpty()!! &&
                binding?.poliTujuan?.selectedItem.toString() != "-- pilih --" &&
                binding?.keperluanTujuan?.selectedItem.toString() != "-- pilih --"

            ) {
                daftarkan(data)
            }
        }


        return binding?.root
    }


    fun inputData() {
        binding?.etTanggalDaftar?.setOnClickListener {
            openDateTime()
        }

        binding?.etTanggalDaftar?.setOnFocusChangeListener { v, hasFocus ->
            if (hasFocus) {
                openDateTime()
            }
        }

        binding?.closeTanggal?.setOnClickListener {
            binding?.errorTanggalDaftar?.text = ""
        }


        binding?.etTanggalDaftar?.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                if (s.isNullOrEmpty()) {
                    binding?.closeTanggal?.visibility = View.GONE
                } else {
                    binding?.closeTanggal?.visibility = View.VISIBLE
                    binding?.errorTanggalDaftar?.text = ""
                }
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {}
        })


        val poliklinik = resources.getStringArray(R.array.Poliklinik)
        val keperluan = resources.getStringArray(R.array.Keperluan)
        val rujukan = resources.getStringArray(R.array.Rujukan)
        val adapterPoliklinik = ArrayAdapter(
            requireContext(),
            android.R.layout.simple_spinner_dropdown_item,
            poliklinik
        )
        val adapterKeperluan = ArrayAdapter(
            requireContext(),
            android.R.layout.simple_spinner_dropdown_item,
            keperluan
        )

        val adapterRujukan = ArrayAdapter(
            requireContext(),
            android.R.layout.simple_spinner_dropdown_item,
            rujukan
        )


        binding?.jenisRujukan?.adapter = adapterRujukan
        binding?.poliTujuan?.adapter = adapterPoliklinik
        binding?.keperluanTujuan?.adapter = adapterKeperluan


        binding?.jenisRujukan?.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {}

            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                when(position) {
                    0 -> {
                        JENIS_RUJUKAN = ""
                        binding?.cardUploadBerkas?.visibility = View.GONE
                    }

                    1 -> {
                        JENIS_RUJUKAN = "LAMA"
                        binding?.cardUploadBerkas?.visibility = View.VISIBLE
                        binding?.btnResumeMedis?.visibility = View.VISIBLE
                        binding?.btnRujukanLama?.visibility = View.VISIBLE
                        binding?.btnSuratRujukan?.visibility = View.GONE

                    }

                    2 -> {
                        JENIS_RUJUKAN = "BARU"
                        binding?.cardUploadBerkas?.visibility = View.VISIBLE
                        binding?.btnSuratRujukan?.visibility = View.VISIBLE
                        binding?.btnRujukanLama?.visibility = View.GONE
                        binding?.btnResumeMedis?.visibility = View.GONE
                    }
                }
            }

        }


        binding?.btnSuratRujukan?.setOnClickListener {
            openCameraRujukan()
        }

        binding?.btnResumeMedis?.setOnClickListener {
            openCameraResume()
        }

        binding?.btnRujukanLama?.setOnClickListener {
            openCameraRujukanLama()
        }




    }


    private fun openCameraRujukan() {
        Intent(MediaStore.ACTION_IMAGE_CAPTURE).also { takePictureIntent ->
            takePictureIntent.resolveActivity(requireContext().packageManager)?.also {
                startActivityForResult(takePictureIntent, REQUEST_IMAGE_RUJUKAN)
            }
        }
    }

    private fun openCameraResume() {
        Intent(MediaStore.ACTION_IMAGE_CAPTURE).also { takePictureIntent ->
            takePictureIntent.resolveActivity(requireContext().packageManager)?.also {
                startActivityForResult(takePictureIntent, REQUEST_IMAGE_RESUME)
            }
        }
    }

    private fun openCameraRujukanLama() {
        Intent(MediaStore.ACTION_IMAGE_CAPTURE).also { takePictureIntent ->
            takePictureIntent.resolveActivity(requireContext().packageManager)?.also {
                startActivityForResult(takePictureIntent, REQUEST_IMAGE_RUJUKAN_LAMA)
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if(resultCode == Activity.RESULT_OK) {

            when(requestCode) {
                REQUEST_IMAGE_RUJUKAN -> {
                    val imageBitmap = data?.extras?.get("data") as Bitmap
                    uploadFile(imageBitmap, "RUJUKAN")
                }

                REQUEST_IMAGE_RESUME -> {
                    val imageBitmap = data?.extras?.get("data") as Bitmap
                    uploadFile(imageBitmap, "KONTROL")
                }

                REQUEST_IMAGE_RUJUKAN_LAMA -> {
                    val imageBitmap = data?.extras?.get("data") as Bitmap
                    uploadFile(imageBitmap, "RUJUKAN_LAMA")
                }

            }

        }
    }


    private fun uploadFile(bitmap: Bitmap, jenis:String) {
        val randomKey = UUID.randomUUID().toString()
        val baos = ByteArrayOutputStream()
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos)
        val data = baos.toByteArray()
        val NAMA = pendaftaranViewModel.NAMA.value.toString()
        val NORM = pendaftaranViewModel.NORM.value.toString()
        val photoRef = storageRef.child("$jenis/${NAMA}_${NORM}_$randomKey")
        photoRef.putBytes(data).addOnSuccessListener { p0 ->
            when(jenis) {
                "RUJUKAN" -> {
                    FOTO_RUJUKAN =  p0?.metadata?.name.toString()
                    binding?.loadingRUJUKAN?.visibility = View.GONE
                    binding?.imgRujukan?.load(bitmap) {
                        crossfade(true)
                        size(150, 150)
                        transformations(RoundedCornersTransformation(10F, 10F, 10F, 10F))
                    }
                }

                "KONTROL" -> {
                    FOTO_RESUME = p0?.metadata?.name.toString()
                    binding?.loadingRESUME?.visibility = View.GONE
                    binding?.imgResume?.load(bitmap) {
                        crossfade(true)
                        size(150, 150)
                        transformations(RoundedCornersTransformation(10F, 10F, 10F, 10F))
                    }
                }

                "RUJUKAN_LAMA" -> {
                    FOTO_RUJUKAN_LAMA = p0?.metadata?.name.toString()
                    binding?.loadingRUJUKANLAMA?.visibility = View.GONE
                    binding?.imgRujukanLama?.load(bitmap) {
                        crossfade(true)
                        size(150, 150)
                        transformations(RoundedCornersTransformation(10F, 10F, 10F, 10F))
                    }
                }
            }
        }.addOnFailureListener {
            Toast.makeText(requireContext(), it.message.toString(), Toast.LENGTH_SHORT).show()
        }.addOnProgressListener(object : OnProgressListener<UploadTask.TaskSnapshot> {
            override fun onProgress(snapshot: UploadTask.TaskSnapshot) {
                when(jenis) {
                    "RUJUKAN" -> {
                        binding?.loadingRUJUKAN?.visibility = View.VISIBLE
                    }

                    "KONTROL" -> {
                        binding?.loadingRESUME?.visibility = View.VISIBLE
                    }

                    "RUJUKAN_LAMA" -> {
                        binding?.loadingRUJUKANLAMA?.visibility = View.VISIBLE
                    }
                }
            }

        })
    }



    @SuppressLint("NewApi")
    private fun openDateTime() {
        val calendar = Calendar.getInstance()
        datePickerDialog = DatePickerDialog(
            requireContext(),
            DatePickerDialog.OnDateSetListener() { _: DatePicker, i: Int, i1: Int, i2: Int ->
                val selectDate = Calendar.getInstance()
                selectDate.set(Calendar.YEAR, i)
                selectDate.set(Calendar.MONTH, i1)
                selectDate.set(Calendar.DAY_OF_MONTH, i2)
                binding?.etTanggalDaftar?.setText(dateFormater.format(selectDate.time))

            },
            calendar.get(Calendar.YEAR),
            calendar.get(Calendar.MONTH),
            calendar.get(Calendar.DAY_OF_MONTH)
        )

        datePickerDialog.show()

    }

    @SuppressLint("SimpleDateFormat")
    fun daftarkan(data: PendaftaranRequest) {
        progressLoading.show()
        progressLoading.setContentView(R.layout.dialog_loading)
        val formatDate = SimpleDateFormat("dd/MM/yyyy", Locale.ROOT)
        val parse = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss")
        CloudApi().daftarkan(data).enqueue(object : Callback<Pendaftaran> {
            override fun onResponse(call: Call<Pendaftaran>, response: Response<Pendaftaran>) {
                if (response.isSuccessful) {
                    if (response.code() == 201) {
                        progressLoading.dismiss()
                        val i = Intent(requireContext(), StrukPendaftaran::class.java)
                        i.putExtra("NO_REGIS", response.body()?.data?.NO_REGIS)
                        i.putExtra("TGL_BOOKING",response.body()?.data?.TGL_BOOKING)
                        i.putExtra("NORM", response.body()?.data?.NORM)
                        i.putExtra("NIK", response.body()?.data?.NIK)
                        i.putExtra("NAMA", pendaftaranViewModel.NAMA.value.toString())
                        i.putExtra("NO_BPJS", NOBPJS)
                        i.putExtra("POLI", response.body()?.data?.POLI)
                        i.putExtra("KEPERLUAN", response.body()?.data?.KEPERLUAN)
                        i.putExtra("KEPESERTAAN", response.body()?.data?.KEPESERTAAN)
                        i.putExtra("NO_SEP", response.body()?.data?.NO_SEP)
                        i.putExtra("STATUS", response.body()?.data?.STATUS)
                        activity?.startActivity(i)
                        activity?.finish()
                    }
                }


                if (response.code() == 200) {
                    if(response.body()?.success == false) {
                        Toast.makeText(requireContext(), response.body()?.message.toString(), Toast.LENGTH_SHORT).show()
                    }
                    progressLoading.dismiss()
                }

            }

            override fun onFailure(call: Call<Pendaftaran>, t: Throwable) {
                progressLoading.dismiss()
                Toast.makeText(requireContext(), t.message, Toast.LENGTH_SHORT).show()
            }
        })
    }

}