 package com.rsdunda.layananmandiri.ui.fragments.Pendaftaran

import android.annotation.SuppressLint
import android.app.DownloadManager
import android.app.ProgressDialog
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Environment
import android.view.MenuItem
import android.view.View
import android.widget.LinearLayout
import android.widget.TextView
import android.widget.Toast
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.observe
import com.google.android.material.card.MaterialCardView
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.zxing.integration.android.IntentIntegrator
import com.rsdunda.layananmandiri.MainActivity
import com.rsdunda.layananmandiri.R
import com.rsdunda.layananmandiri.ui.activity.ScanQrCode
import com.rsdunda.layananmandiri.ui.viewmodel.InfoFragmentViewModel
import com.rsdunda.layananmandiri.util.toast
import java.text.SimpleDateFormat
import java.util.*
import java.util.jar.Manifest

@Suppress("NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS")
class StrukPendaftaran : AppCompatActivity() {
    lateinit var textTanggal:TextView
    lateinit var textNoRegis:TextView
    lateinit var textPeserta:TextView
    lateinit var textNoRm:TextView
    lateinit var textNik:TextView
    lateinit var textNama:TextView
    lateinit var textPoli:TextView
    lateinit var textKeperluan:TextView
    lateinit var msgPending:TextView
    lateinit var msgDiterima:TextView
    lateinit var textPesan:TextView
    lateinit var msgTolak:TextView
    lateinit var buttonQRCODE:FloatingActionButton
    lateinit var card_button_donwload: LinearLayout
    lateinit var btn_download: MaterialCardView
    private val formatDate = SimpleDateFormat("dd-MM-yyyy", Locale.ROOT)
    private val STORAGE_PERMISSION_CODE = 1000
    var FILE_URI = ""
    @SuppressLint("SimpleDateFormat")
    private val parse = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss")
    private lateinit var initScan: IntentIntegrator
    private lateinit var infoFragmentViewModel: InfoFragmentViewModel
    private lateinit var progressLoading: ProgressDialog
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_struk_pendaftaran)
        infoFragmentViewModel = ViewModelProvider(this).get(InfoFragmentViewModel::class.java)
//        supportActionBar?.hide()
        supportActionBar?.title = "Bukti Pendaftaran"
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setHomeButtonEnabled(true)
        actionBar?.title = "Bukti Pendaftaran"

        initScan = IntentIntegrator(this)
        progressLoading = ProgressDialog(this)
        progressLoading.window?.setBackgroundDrawableResource(android.R.color.transparent)

        textTanggal = findViewById(R.id.textTanggal)
        textNoRegis = findViewById(R.id.textNoRegis)
        textPeserta = findViewById(R.id.textPeserta)
        textNoRm = findViewById(R.id.textNoRm)
        textNik = findViewById(R.id.textNik)
        textNama = findViewById(R.id.textNama)
        textPoli = findViewById(R.id.textPoli)
        textPesan = findViewById(R.id.textPesan)
        textKeperluan = findViewById(R.id.textKeperluan)
        msgPending = findViewById(R.id.msgPending)
        msgDiterima = findViewById(R.id.msgDiterima)
        msgTolak = findViewById(R.id.msgTolak)
        buttonQRCODE = findViewById(R.id.buttonQRCODE)
        card_button_donwload = findViewById(R.id.card_button_donwload)
        btn_download = findViewById(R.id.btn_download)

        initLoad()


        buttonQRCODE.setOnClickListener {
            initScan.setCaptureActivity(ScanQrCode::class.java)
            initScan.setDesiredBarcodeFormats(IntentIntegrator.QR_CODE);
            initScan.setPrompt("");
            initScan.setCameraId(0);
            initScan.setBeepEnabled(true);
            initScan.setBarcodeImageEnabled(true);
            initScan.setOrientationLocked(false);
            initScan.initiateScan();
        }

    }

    private fun initLoad() {
        FILE_URI = intent.getStringExtra("NO_SEP").toString()
        textNoRegis.text = intent.getStringExtra("NO_REGIS")
//        textTanggal.text = formatDate.format(parse.parse(intent.getStringExtra("TGL_BOOKING")))
        textTanggal.text =intent.getStringExtra("TGL_BOOKING")
        textNoRm.text = intent.getStringExtra("NORM")
        textNik.text = intent.getStringExtra("NIK")
        textNama.text = intent.getStringExtra("NAMA")
        textPoli.text = intent.getStringExtra("POLI")
        textKeperluan.text = intent.getStringExtra("KEPERLUAN")
        textPeserta.text = intent.getStringExtra("KEPESERTAAN")
        val PESAN = intent.getStringExtra("PESAN")
        val STATUS = intent.getStringExtra("STATUS")
        val KEPESERTAAN = intent.getStringExtra("KEPESERTAAN")
        val NO_REGIS = intent.getStringExtra("NO_REGIS")

        if(KEPESERTAAN == "BPJS") {
            buttonQRCODE.visibility = View.VISIBLE
            card_button_donwload.visibility = View.VISIBLE
        } else {
            buttonQRCODE.visibility = View.GONE
            card_button_donwload.visibility = View.GONE
        }


        when(STATUS) {
            "0" -> {
                msgPending.visibility = View.VISIBLE
                msgDiterima.visibility = View.GONE
                msgTolak.visibility = View.GONE
                buttonQRCODE.visibility = View.GONE
                textPesan.text = "Pendaftaran anda sementara diproses, silahkan menunggu di rumah hingga status pendaftaran DITERIMA"
                btn_download.visibility = View.GONE
            }
            "1" -> {
                msgPending.visibility = View.GONE
                msgDiterima.visibility = View.VISIBLE
                msgTolak.visibility = View.GONE
                if(KEPESERTAAN == "BPJS") {
                    textPesan.text = "Selamat Anda terdaftar sebagai pasien BPJS, Silahkan Cetak SEP dengan Scan QR Code pada Anjunan Mandiri di Rumah Sakit"
                    btn_download.visibility = View.VISIBLE
                } else {
                    textPesan.text = "Selamat Anda terdaftar sebagai pasien UMUM, Silahkan datang ke Rumah Sakit dengan menunjukan bukti pendaftaran ini ke Kasir sebelum mendapat pelayanan kesehatan"
                    btn_download.visibility = View.GONE
                }
            }
            "2" -> {
                msgPending.visibility = View.GONE
                msgDiterima.visibility = View.GONE
                msgTolak.visibility = View.VISIBLE
                buttonQRCODE.visibility = View.GONE
                textPesan.text = "Maaf pendaftaran Anda di tolak. $PESAN"
                btn_download.visibility = View.GONE
            }

        }

        btn_download.setOnClickListener {
            if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if(checkSelfPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_DENIED) {
                    requestPermissions(arrayOf(android.Manifest.permission.WRITE_EXTERNAL_STORAGE), STORAGE_PERMISSION_CODE)
                } else {
                    startDownloading()
                }
            }else {
                startDownloading()
            }
        }

//        buttonQRCODE.setOnClickListener {
//            val i = Intent(this@StrukPendaftaran, QrCode::class.java)
//            i.putExtra("NO_SEP", FILE)
//            i.putExtra("NO_REGIS", NO_REGIS)
//            startActivity(i)
//        }

        infoFragmentViewModel.isLoading.observe(this@StrukPendaftaran, androidx.lifecycle.Observer {
            if(it) {
                progressLoading.show()
                progressLoading.setContentView(R.layout.dialog_loading)
            } else {
                progressLoading.dismiss()
            }
        })
    }

    private fun startDownloading() {
        val request = DownloadManager.Request(Uri.parse(FILE_URI))
        request.setAllowedNetworkTypes(DownloadManager.Request.NETWORK_WIFI or DownloadManager.Request.NETWORK_MOBILE)
        request.setTitle("Download file SEP")
        request.setDescription("File sementara di download")
        request.allowScanningByMediaScanner()
        request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED)
        request.setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS, "${System.currentTimeMillis()}")
        val manager  = getSystemService(Context.DOWNLOAD_SERVICE) as DownloadManager
        manager.enqueue(request)
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when(requestCode) {
            STORAGE_PERMISSION_CODE -> {
                if(grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    startDownloading()
                }
            }
            else -> {
                toast("Permission Denied")
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        val result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
        if(result != null) {
            if(result.contents == null) {
                toast("Silahkan coba lagi")
            } else {
                Toast.makeText(this@StrukPendaftaran, result.contents, Toast.LENGTH_SHORT).show()
                infoFragmentViewModel.scanStrukPendaftaran("${result.contents}-${intent.getStringExtra("NO_REGIS")}")
            }
        }
        super.onActivityResult(requestCode, resultCode, data)

    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        super.onOptionsItemSelected(item)
        when(item.itemId) {
            android.R.id.home -> {
                onBackPressed()
            }
        }
        return true
    }

    override fun onBackPressed() {
        super.onBackPressed()
        val intent = Intent(this@StrukPendaftaran, MainActivity::class.java)
        startActivity(intent)
        finish()
    }
}