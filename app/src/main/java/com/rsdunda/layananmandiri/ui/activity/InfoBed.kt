package com.rsdunda.layananmandiri.ui.activity

import android.app.ProgressDialog
import android.app.SearchManager
import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.widget.SearchView
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.rsdunda.layananmandiri.MainActivity
import com.rsdunda.layananmandiri.R
import com.rsdunda.layananmandiri.ui.adapter.KamarAdapter
import com.rsdunda.layananmandiri.ui.viewmodel.BedViewModel

class InfoBed : AppCompatActivity() {
    lateinit var rv_kamar: RecyclerView
    lateinit var swipeKamar: SwipeRefreshLayout
    private lateinit var kamarAdapter: KamarAdapter
    private lateinit var bedViewModel: BedViewModel
    private lateinit var progressLoading: ProgressDialog
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_info_bed)
        supportActionBar?.title = "Informasi Kamar"
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setHomeButtonEnabled(true)
        actionBar?.title = "Informasi Kamar"

        bedViewModel = ViewModelProvider(this).get(BedViewModel::class.java)
        kamarAdapter = KamarAdapter(mutableListOf(), this@InfoBed, bedViewModel)
        rv_kamar = findViewById(R.id.rv_kamar)
        swipeKamar = findViewById(R.id.swipeKamar)
        progressLoading = ProgressDialog(this@InfoBed)
        progressLoading.window?.setBackgroundDrawableResource(android.R.color.transparent)


        rv_kamar?.apply {
            layoutManager = LinearLayoutManager(this@InfoBed)
            adapter = kamarAdapter
        }

        swipeKamar.setOnRefreshListener {
            bedViewModel.getDataBed()
        }



        initLoad()

    }


    private fun initLoad() {
        bedViewModel.getDataBed()
        swipeKamar.isRefreshing = true

        bedViewModel.isLoading.observe(this, Observer {
            if(it) {
                progressLoading.show()
                progressLoading.setContentView(R.layout.dialog_loading)
            } else {
                progressLoading.dismiss()
            }
        })

        bedViewModel.listenKamar().observe(this, Observer {
            if(it != null) {
                swipeKamar.isRefreshing = false
                rv_kamar.adapter?.let { a ->
                    if(a is KamarAdapter) {
                        a.updateList(it)
                    }
                }
            }
        })
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_cari, menu)
        val menuItem = menu?.findItem(R.id.btn_cari)
        val searchMenu = getSystemService(Context.SEARCH_SERVICE) as SearchManager
        val searchView: SearchView? = menuItem?.actionView as SearchView
        searchView?.queryHint = "Cari Bed..."
        searchView?.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                return false
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                kamarAdapter.filter.filter(newText)
                return true
            }

        })
        return super.onCreateOptionsMenu(menu)

    }


    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when(item.itemId) {
            android.R.id.home -> {
                onBackPressed()
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onBackPressed() {
        super.onBackPressed()
        finish()
        val intent = Intent(this@InfoBed, MainActivity::class.java)
        startActivity(intent)

    }
}