package com.rsdunda.layananmandiri.ui.auth

import android.content.Context
import androidx.datastore.core.DataStore
import androidx.datastore.preferences.core.*
import androidx.datastore.preferences.createDataStore
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.map
import java.io.IOException


class AuthData(context: Context) {
    private val PREFERENCE_NAME = "auth_preference"

    private object PreferenceKeys {
        val fullname = stringPreferencesKey("fullname")
        val isAuth = booleanPreferencesKey("is_auth")
        val uid = stringPreferencesKey("uid")
        val nomor_hp = stringPreferencesKey("nomor_hp")
        val norm = stringPreferencesKey("norm")
        val nik = stringPreferencesKey("nik")
        val kelamin = stringPreferencesKey("kelamin")
        val foto = stringPreferencesKey("foto")
        val alamat = stringPreferencesKey("alamat")
        val role = stringPreferencesKey("role")
    }


    private val dataStore: DataStore<Preferences> = context.createDataStore(
            name = PREFERENCE_NAME
    )

//    private const val UID = "UID"
//    private const val NOHP = "NOHP"
//    private const val NORM = "NORM"
//    private const val NIK = "NIK"
//    private const val FULLNAME = "FULLNAME"
//    private const val KELAMIN = "KELAMIN"
//    private const val FOTO = "FOTO"
//    private const val ALAMAT = "ALAMAT"
//    private const val AUTH = "AUTH"
//    private const val CHECK = "CHECK"
//    private const val ROLE = "ROLE"


    suspend fun saveToDataStore(
            uid: String,
            nomor_hp: String,
            norm: String,
            nik: String,
            fullname: String,
            kelamin: String,
            foto: String,
            alamat: String,
            role: String,
            isAuth: Boolean
    ) {
        dataStore.edit { preference ->
            preference[PreferenceKeys.uid] = uid
            preference[PreferenceKeys.nomor_hp] = nomor_hp
            preference[PreferenceKeys.norm] = norm
            preference[PreferenceKeys.nik] = nik
            preference[PreferenceKeys.fullname] = fullname
            preference[PreferenceKeys.kelamin] = kelamin
            preference[PreferenceKeys.foto] = foto
            preference[PreferenceKeys.alamat] = alamat
            preference[PreferenceKeys.role] = role
            preference[PreferenceKeys.isAuth] = isAuth
        }
    }

//    val uidStore: Flow<String> = dataStore.data
//        .catch { exception ->
//            if(exception is IOException) {
//                emit(emptyPreferences())
//            } else {
//                throw  exception
//            }
//        }
//        .map { preference ->
//            val uid = preference[PreferenceKeys.uid] ?: ""
//            uid
//
//        }


    val readFromDataStore: Flow<String> = dataStore.data
            .catch { exception ->
                if (exception is IOException) {
                    emit(emptyPreferences())
                } else {
                    throw  exception
                }
            }
            .map { preference ->
                val fullname = preference[PreferenceKeys.fullname] ?: ""
                val uid = preference[PreferenceKeys.uid] ?: ""
                fullname
            }

}