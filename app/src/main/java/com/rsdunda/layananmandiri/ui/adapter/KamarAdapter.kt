package com.rsdunda.layananmandiri.ui.adapter

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Filter
import android.widget.Filterable
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.card.MaterialCardView
import com.rsdunda.layananmandiri.R
import com.rsdunda.layananmandiri.data.model.kamar.Kamar
import com.rsdunda.layananmandiri.ui.viewmodel.BedViewModel
import java.util.*
import kotlin.collections.ArrayList

class KamarAdapter(
    private var kamar:MutableList<Kamar>,
    private var context: Context,
    private var bedViewModel: BedViewModel
): RecyclerView.Adapter<KamarAdapter.ViewHolder>(), Filterable {
    var kamarList = ArrayList<Kamar>()
    init {
        kamarList = kamar as ArrayList<Kamar>
    }

    class ViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {
        val nama_kamar = itemView.findViewById<TextView>(R.id.nama_kamar)
        val detail = itemView.findViewById<TextView>(R.id.detail)
        val status = itemView.findViewById<TextView>(R.id.status)
        val card_bed = itemView.findViewById<MaterialCardView>(R.id.card_bed)

        @SuppressLint("SetTextI18n")
        fun bind(data: Kamar, context: Context, bedViewModel: BedViewModel) {
            nama_kamar.text = data.RUANGAN
            detail.text = "${data.KAMAR}. ${data.TEMPAT_TIDUR} / ${data.KELAS}"
            status.text = data.STATUS

            when(data.STATUS) {
                "Kosong" -> {
                   card_bed.setCardBackgroundColor(ContextCompat.getColor(context, R.color.white))
                }

                "Terisi" -> {
                    card_bed.setCardBackgroundColor(ContextCompat.getColor(context, R.color.color_error))
                }
            }
        }
    }

    fun updateList(data: List<Kamar>) {
        kamar.clear()
        kamar.addAll(data)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.bed_items, parent, false))
    }

    override fun getItemCount() = kamarList.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) = holder.bind(kamarList[position], context, bedViewModel)


    override fun getFilter(): Filter {
        return  object : Filter() {
            override fun performFiltering(constraint: CharSequence?): FilterResults {
                val cari = constraint.toString()
                if(cari.isEmpty()) {
                    kamarList = kamar as ArrayList<Kamar>
                } else {
                    val resultList = ArrayList<Kamar>()
                    for (row in kamar) {
                        if(row.RUANGAN.toLowerCase(Locale.ROOT).contains(cari.toLowerCase(Locale.ROOT)) || row.STATUS.toLowerCase(
                                Locale.ROOT).contains(cari.toLowerCase(Locale.ROOT))) {
                            resultList.add(row)
                        }
                    }

                    kamarList = resultList
                }

                val filterResult = FilterResults()
                filterResult.values = kamarList
                return  filterResult
            }

            override fun publishResults(constraint: CharSequence?, results: FilterResults?) {
                kamarList = results?.values as ArrayList<Kamar>
                notifyDataSetChanged()
            }

        }
    }
}