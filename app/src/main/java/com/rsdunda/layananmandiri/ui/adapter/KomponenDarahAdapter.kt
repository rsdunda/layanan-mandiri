package com.rsdunda.layananmandiri.ui.adapter

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.card.MaterialCardView
import com.google.gson.Gson
import com.rsdunda.layananmandiri.R
import com.rsdunda.layananmandiri.data.model.darah.Darah
import com.rsdunda.layananmandiri.data.model.darah.Komponen
import com.rsdunda.layananmandiri.data.model.darah.KomponenView
import com.rsdunda.layananmandiri.ui.viewmodel.DarahViewModel

class KomponenDarahAdapter(private var komponenDarah:MutableList<Komponen>, private var context: Context, private var darahViewModel: DarahViewModel): RecyclerView.Adapter<KomponenDarahAdapter.ViewHolder> () {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_darah, parent,false))
    }

    override fun getItemCount() = komponenDarah.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) = holder.bind(komponenDarah[position], context, darahViewModel)

    fun updateList(data: List<Komponen>) {
        komponenDarah.clear()
        komponenDarah.addAll(data)
        notifyDataSetChanged()
    }

    class ViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {
        val txt_nama_komponen = itemView.findViewById<TextView>(R.id.txt_nama_komponen)
        val txt_stok = itemView.findViewById<TextView>(R.id.txt_stok)
        fun bind(data:Komponen, context: Context, darahViewModel: DarahViewModel) {
            txt_nama_komponen.text = data.nama_komponen
            txt_stok.text = data.stok.toString()
        }
    }


}
