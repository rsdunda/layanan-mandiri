package com.rsdunda.layananmandiri.ui.activity

import android.app.ProgressDialog
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.rsdunda.layananmandiri.MainActivity
import com.rsdunda.layananmandiri.R
import com.rsdunda.layananmandiri.data.model.jadwal.Jadwal
import com.rsdunda.layananmandiri.ui.adapter.JadwalAdapter
import com.rsdunda.layananmandiri.ui.viewmodel.JadwalViewModel

class JadwalDokter : AppCompatActivity() {
    private lateinit var jadwalViewModel: JadwalViewModel
    lateinit var rv_jadwal: RecyclerView
    private lateinit var swipeJadwal: SwipeRefreshLayout
    lateinit var jadwalAdapter: JadwalAdapter
    private lateinit var progressLoading: ProgressDialog
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_jadwal_dokter)
        supportActionBar?.title = "Jadwal Dokter"
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setHomeButtonEnabled(true)
        actionBar?.title = "Jadwal Dokter"

        jadwalViewModel = ViewModelProvider(this).get(JadwalViewModel::class.java)
        jadwalAdapter = JadwalAdapter(mutableListOf(), this@JadwalDokter, jadwalViewModel)

        progressLoading = ProgressDialog(this@JadwalDokter)
        progressLoading.window?.setBackgroundDrawableResource(android.R.color.transparent)

        rv_jadwal = findViewById(R.id.rv_jadwal)
        swipeJadwal = findViewById(R.id.swipeJadwal)
        rv_jadwal.apply {
            layoutManager = LinearLayoutManager(this@JadwalDokter)
            adapter = jadwalAdapter
        }

       initLoad()
    }

    private fun initLoad() {
        jadwalViewModel.getDataJadwal()
        jadwalViewModel.isLoading.observe(this, Observer {
            if (it) {
                progressLoading.show()
                progressLoading.setContentView(R.layout.dialog_loading)
            } else {
                progressLoading.dismiss()
            }
        })
        jadwalViewModel.listenJadwal().observe(this, Observer {
            if(it != null) {
                rv_jadwal.adapter?.let { a ->
                    if(a is JadwalAdapter) {
                        a.setData(it)
                    }
                }
            }
        })
    }


    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        super.onOptionsItemSelected(item)
        when(item.itemId) {
            android.R.id.home -> {
                onBackPressed()
            }
        }
        return true
    }

    override fun onBackPressed() {
        super.onBackPressed()
        finish()
        val intent = Intent(this@JadwalDokter, MainActivity::class.java)
        startActivity(intent)

    }
}
