package com.rsdunda.layananmandiri.ui.fragments.FormProfile

import android.annotation.SuppressLint
import android.app.AlertDialog
import android.app.DatePickerDialog
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.DatePicker
import android.widget.RadioButton
import android.widget.Toast
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import com.rsdunda.layananmandiri.R
import com.rsdunda.layananmandiri.databinding.FragmentBioBinding
import com.rsdunda.layananmandiri.ui.auth.AuthViewModel
import com.rsdunda.layananmandiri.util.SharedUsers
import java.text.SimpleDateFormat
import java.util.*

class BioFragment : Fragment() {
    private var _binding: FragmentBioBinding? = null
    private val binding get() = _binding
    private val authViewModel: AuthViewModel by activityViewModels()
    private lateinit var datePickerDialog: DatePickerDialog
    private lateinit var dateFormater: SimpleDateFormat
    private var kelamin: String? = null
    private lateinit var sharedUsers: SharedUsers


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        _binding = FragmentBioBinding.inflate(inflater, container, false)
        dateFormater = SimpleDateFormat("dd-MM-yyyy", Locale.ROOT)
        sharedUsers = SharedUsers(requireContext())
        inputUtils()
        lanjutkan()
        return binding?.root
    }


    private fun inputUtils() {
        binding?.etTanggalLahir?.setOnFocusChangeListener { v, hasFocus ->
            if (hasFocus) {
                openDateTime()
            }
        }

        binding?.etTanggalLahir?.setOnClickListener {
            openDateTime()
        }

        binding?.closeNik?.setOnClickListener {
            binding?.etNik?.text?.clear()
        }

        binding?.closeNama?.setOnClickListener {
            binding?.etNama?.text?.clear()
        }

        binding?.closeTempat?.setOnClickListener {
            binding?.etTempatLahir?.text?.clear()
        }

        binding?.closeTanggal?.setOnClickListener {
            binding?.etTanggalLahir?.text?.clear()
        }

        binding?.closeAlamat?.setOnClickListener {
            binding?.etAlamat?.text?.clear()
        }

        binding?.closeKelurahan?.setOnClickListener {
            binding?.etKelurahan?.text?.clear()
        }

        binding?.closeKecamatan?.setOnClickListener {
            binding?.etKecamatan?.text?.clear()
        }


        binding?.etNik?.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                if (s.isNullOrEmpty()) {
                    binding?.closeNik?.visibility = View.GONE
                } else {
                    binding?.closeNik?.visibility = View.VISIBLE
                    binding?.errorNik?.text = ""
                }
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {}
        })

        binding?.etNama?.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                if (s.isNullOrEmpty()) {
                    binding?.closeNama?.visibility = View.GONE
                } else {
                    binding?.closeNama?.visibility = View.VISIBLE
                    binding?.errorNama?.text = ""
                }
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {}
        })

        binding?.etTempatLahir?.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                if (s.isNullOrEmpty()) {
                    binding?.closeTempat?.visibility = View.GONE
                } else {
                    binding?.closeTempat?.visibility = View.VISIBLE
                    binding?.errorTempatLahir?.text = ""
                }
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {}
        })

        binding?.etTanggalLahir?.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                if (s.isNullOrEmpty()) {
                    binding?.closeTanggal?.visibility = View.GONE
                } else {
                    binding?.closeTanggal?.visibility = View.VISIBLE
                    binding?.errorTanggalLahir?.text = ""
                }
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {}
        })

        binding?.etAlamat?.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                if (s.isNullOrEmpty()) {
                    binding?.closeAlamat?.visibility = View.GONE
                } else {
                    binding?.closeAlamat?.visibility = View.VISIBLE
                    binding?.errorAlamat?.text = ""
                }
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {}
        })

        binding?.etKelurahan?.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                if (s.isNullOrEmpty()) {
                    binding?.closeKelurahan?.visibility = View.GONE
                } else {
                    binding?.closeKelurahan?.visibility = View.VISIBLE
                }
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {}
        })

        binding?.etKecamatan?.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                if (s.isNullOrEmpty()) {
                    binding?.closeKecamatan?.visibility = View.GONE
                } else {
                    binding?.closeKecamatan?.visibility = View.VISIBLE
                }
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {}
        })
    }


    @SuppressLint("NewApi")
    private fun openDateTime() {
        val calendar = Calendar.getInstance()
        datePickerDialog = DatePickerDialog(requireContext(), DatePickerDialog.OnDateSetListener() { datePicker: DatePicker, i: Int, i1: Int, i2: Int ->
            val selectDate = Calendar.getInstance()
            selectDate.set(Calendar.YEAR, i)
            selectDate.set(Calendar.MONTH, i1)
            selectDate.set(Calendar.DAY_OF_MONTH, i2)
            binding?.etTanggalLahir?.setText(dateFormater.format(selectDate.time))

        }, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH))

        datePickerDialog.show()

    }

    @SuppressLint("SetTextI18n")
    private fun lanjutkan() {
        binding?.btnNext?.setOnClickListener {
            if (binding?.etNik?.text.isNullOrEmpty()) {
                Toast.makeText(requireContext(), "Oops!. Inputan belum lengkap", Toast.LENGTH_SHORT).show()
                binding?.errorNik?.text = "NIK wajib diisi"
                binding?.etNik?.requestFocus()
                return@setOnClickListener
            }

            if (binding?.etNik?.text?.length!! < 16) {
                Toast.makeText(requireContext(), "Oops!. Inputan belum lengkap", Toast.LENGTH_SHORT).show()
                binding?.errorNik?.text = "Nik anda belum lengkap nih."
                binding?.etNik?.requestFocus()
                return@setOnClickListener
            }


            if (binding?.etNama?.text.isNullOrEmpty()) {
                Toast.makeText(requireContext(), "Oops!. Inputan belum lengkap", Toast.LENGTH_SHORT).show()
                binding?.errorNama?.text = "Nama wajib diisi"
                binding?.etNama?.requestFocus()
                return@setOnClickListener
            }

            if (binding?.etTempatLahir?.text.isNullOrEmpty()) {
                Toast.makeText(requireContext(), "Oops!. Inputan belum lengkap", Toast.LENGTH_SHORT).show()
                binding?.errorTempatLahir?.text = "Tempat Lahir anda wajib diisi"
                binding?.etTempatLahir?.requestFocus()
                return@setOnClickListener
            }

            if (binding?.etTanggalLahir?.text.isNullOrEmpty()) {
                Toast.makeText(requireContext(), "Oops!. Inputan belum lengkap", Toast.LENGTH_SHORT).show()
                binding?.errorTanggalLahir?.text = "Tanggal Lahir wajib diisi"
                binding?.etTanggalLahir?.requestFocus()
                return@setOnClickListener
            }

            if (binding?.rKelamin?.checkedRadioButtonId != -1) {
                binding?.errorKelamin?.text = ""
                val jKelamin = binding?.rKelamin?.checkedRadioButtonId?.let { it1 -> binding?.root?.findViewById<RadioButton>(it1) }
                kelamin = jKelamin?.text.toString()
            } else {
                Toast.makeText(requireContext(), "Oops!. Jenis kelamin belum di pilih", Toast.LENGTH_SHORT).show()
                binding?.errorKelamin?.text = "Kelamin belum di pilih"
                return@setOnClickListener
            }

            if (binding?.etAlamat?.text.isNullOrEmpty()) {
                Toast.makeText(requireContext(), "Oops!. Inputan belum lengkap", Toast.LENGTH_SHORT).show()
                binding?.errorAlamat?.text = "Alamat wajib diisi"
                binding?.etAlamat?.requestFocus()
                return@setOnClickListener
            }

            setDataInput()
            findNavController().navigate(R.id.action_bioFragment_to_bpjsDialogFragment)
        }
    }


    private fun setDataInput() {
        authViewModel.let {
            it.setNik(binding?.etNik?.text.toString())
            it.setNamaLengkap(binding?.etNama?.text.toString())
            it.setTempatLahir(binding?.etTempatLahir?.text.toString())
            it.setTanggalLahir(binding?.etTanggalLahir?.text.toString())
            it.setKelamin(kelamin.toString())
            it.setAlamat(binding?.etAlamat?.text.toString())
            it.setRt(binding?.etRt?.text.toString())
            it.setRw(binding?.etRw?.text.toString())
            it.setKelurahan(binding?.etKelurahan?.text.toString())
            it.setKecamatan(binding?.etKecamatan?.text.toString())
        }

        sharedUsers.let { user ->
            user.nik = binding?.etNik?.text.toString()
            user.nama_lengkap = binding?.etNama?.text.toString()
            user.kelamin = kelamin.toString()
            user.alamat = binding?.etAlamat?.text.toString()
        }
    }


    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }

}