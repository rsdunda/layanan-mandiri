package com.rsdunda.layananmandiri.ui.fragments.Pendaftaran

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.OnBackPressedCallback
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import com.rsdunda.layananmandiri.MainActivity
import com.rsdunda.layananmandiri.R
import com.rsdunda.layananmandiri.databinding.FragmentStep1Binding
import com.rsdunda.layananmandiri.ui.viewmodel.PendaftaranViewModel
import kotlin.reflect.typeOf

class Step1Fragment : Fragment() {
    private var _binding: FragmentStep1Binding? = null
    private val binding get() = _binding
    private val pendaftaranViewModel:PendaftaranViewModel by activityViewModels()

    @SuppressLint("SetTextI18n")
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        _binding = FragmentStep1Binding.inflate(inflater,container, false)
        val PENDAFTAR = activity?.intent?.extras?.getString("PENDAFTAR").toString()
        val KELAMIN = activity?.intent?.extras?.getString("JENIS_KELAMIN").toString()
        if(PENDAFTAR === "SENDIRI") {
            binding?.textPesan?.text = "Silahakan lengkapi formulir pendaftaran anda dengan menekan tombol lanjutkan, pastikan data yang di tampilkan sesuai dengan data anda"
        }
        if(PENDAFTAR === "ORANG"){
            binding?.textPesan?.text = "Silahakan lengkapi formulir pendaftaran anda dengan menekan tombol lanjutkan, pastikan data yang di tampilkan sesuai dengan data pasien yang anda akan daftarkan"
        }

        if(KELAMIN === "1") {
            binding?.textKelamin?.text = "LAKI - LAKI"
        }

        if(KELAMIN === "2") {
            binding?.textKelamin?.text = "PEREMPUAN"
        }

        pendaftaranViewModel.let {
            activity?.intent?.extras?.getString("NORM")?.let { it1 -> it.setNorm(it1) }
            activity?.intent?.extras?.getString("NAMA")?.let { it1 -> it.setNama(it1) }
            activity?.intent?.extras?.getString("JENIS_KELAMIN")?.let { it1 -> it.setKelamin(it1) }
            activity?.intent?.extras?.getString("TANGGAL_LAHIR")?.let { it1 -> it.setTanggalLahir(it1) }
        }

        binding?.textNorm?.text = activity?.intent?.extras?.getString("NORM")
        binding?.textNama?.text = activity?.intent?.extras?.getString("NAMA")
        binding?.textTanggal?.text = activity?.intent?.extras?.getString("TANGGAL_LAHIR")
        binding?.textAlamat?.text = activity?.intent?.extras?.getString("ALAMAT")


        binding?.btnNext?.setOnClickListener {
            findNavController().navigate(R.id.action_step1Fragment_to_step2Fragment)
        }

        binding?.btnBatal?.setOnClickListener {
            context?.startActivity(Intent(requireContext(), MainActivity::class.java))
        }

        return binding?.root
    }

    fun kembali() {
        val callback = object : OnBackPressedCallback(true) {
            override fun handleOnBackPressed() {
//                findNavController().navigate(R.id.action_bpjsFragment_to_kembaliFragment)
            }
        }

        requireActivity().onBackPressedDispatcher.addCallback(callback)
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }

}