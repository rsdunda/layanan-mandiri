package com.rsdunda.layananmandiri.ui.viewmodel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.rsdunda.layananmandiri.data.model.Pasien.Data
import com.rsdunda.layananmandiri.data.model.Pasien.Pasien
import com.rsdunda.layananmandiri.data.repository.PasienRepository
import kotlinx.coroutines.launch

class PendaftaranViewModel(application: Application): AndroidViewModel(application) {

    private val pasienRepository = PasienRepository()
    private var _norm = MutableLiveData<String>()
    private var _nama = MutableLiveData<String>()
    private var _kelamin = MutableLiveData<String>()
    private var _ktp = MutableLiveData<String>()
    private var _tanggal = MutableLiveData<String>()
    private var _tanggal_lahir = MutableLiveData<String>()
    private var _poliklinik = MutableLiveData<String>()
    private var _keperluan = MutableLiveData<String>()
    private var _bpjs = MutableLiveData<String>()

    val pasien: LiveData<Data> = pasienRepository.pasien

    val NORM: LiveData<String> = _norm
    val NAMA: LiveData<String> = _nama
    val BPJS: LiveData<String> = _bpjs
    val KELAMIN: LiveData<String> = _kelamin
    val FOTO_KTP: LiveData<String> = _ktp
    val TANGGAL_BOOKING: LiveData<String> = _tanggal
    val TANGGAL_LAHIR: LiveData<String> = _tanggal_lahir
    val POLIKLINIK: LiveData<String> = _poliklinik
    val KEPERLUAN: LiveData<String> = _keperluan

    fun setTanggalLahir(a:String){
        _tanggal_lahir.value = a
    }

    fun setNorm(norm: String) {
        _norm.value = norm
    }

    fun setNama(nama: String) {
        _nama.value = nama
    }

    fun setBPJS(nomor: String) {
        _bpjs.value = nomor
    }

    fun setKelamin(k:String) {
        _kelamin.value = k
    }

    fun setKtp(a:String) {
        _ktp.value = a
    }

    fun setTanggalBooking(a:String) {
        _tanggal.value = a
    }

    fun setPoliklinik(a: String) {
        _poliklinik.value = a
    }

    fun setKeperluan(a:String) {
        _keperluan.value = a
    }




    fun checkNORM(norm: String) {
        viewModelScope.launch {
            pasienRepository.checkNORM(norm)
        }
    }

}