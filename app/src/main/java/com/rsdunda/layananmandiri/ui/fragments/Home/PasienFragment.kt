package com.rsdunda.layananmandiri.ui.fragments.Home

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.rsdunda.layananmandiri.R
import com.rsdunda.layananmandiri.databinding.FragmentPasienBinding

class PasienFragment : Fragment() {
    private var _binding: FragmentPasienBinding? = null
    private val binding get() = _binding
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        _binding = FragmentPasienBinding.inflate(inflater, container, false)


        return binding?.root
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }


}