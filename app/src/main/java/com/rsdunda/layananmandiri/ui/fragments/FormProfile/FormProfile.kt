package com.rsdunda.layananmandiri.ui.fragments.FormProfile

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.rsdunda.layananmandiri.R
import com.rsdunda.layananmandiri.databinding.ActivityFormProfileBinding

class FormProfile : AppCompatActivity() {
    private lateinit var binding: ActivityFormProfileBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        actionBar?.hide()
        supportActionBar?.hide()

        binding = ActivityFormProfileBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)


    }
}