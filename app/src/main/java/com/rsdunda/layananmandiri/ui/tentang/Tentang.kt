package com.rsdunda.layananmandiri.ui.tentang

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import com.rsdunda.layananmandiri.MainActivity
import com.rsdunda.layananmandiri.R
import com.rsdunda.layananmandiri.ui.activity.JadwalDokter

class Tentang : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_tentang)
        actionBar?.title = "Team Layanan Mandiri Development"
        supportActionBar?.title = "Team Layanan Mandiri Development"
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setHomeButtonEnabled(true)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        super.onOptionsItemSelected(item)
        when(item.itemId) {
            android.R.id.home -> {
                onBackPressed()
            }
        }
        return true
    }

    override fun onBackPressed() {
        super.onBackPressed()
        finish()
        val intent = Intent(this@Tentang, MainActivity::class.java)
        startActivity(intent)

    }

}