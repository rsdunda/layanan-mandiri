package com.rsdunda.layananmandiri.ui.fragments.Home

import android.app.ProgressDialog
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.rsdunda.layananmandiri.R
import com.rsdunda.layananmandiri.data.model.darah.DarahView
import com.rsdunda.layananmandiri.databinding.FragmentDarahBinding
import com.rsdunda.layananmandiri.ui.adapter.DarahAdapter
import com.rsdunda.layananmandiri.ui.viewmodel.InfoViewModel


class DarahFragment : Fragment() {
    private var _binding: FragmentDarahBinding? = null
    private val binding get() = _binding
    private val infoViewModel: InfoViewModel by activityViewModels()
    private lateinit var progressLoading: ProgressDialog
    lateinit var darahAdapter: DarahAdapter
//    lateinit var layoutManager: GridLayoutManager
//    val ln = GridLayoutManager(requireContext(), 3)
    val ln = LinearLayoutManager(requireContext())
    val addDarahList: MutableList<DarahView> = ArrayList()
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        _binding = FragmentDarahBinding.inflate(inflater, container, false)
//        // progres dialog
        progressLoading = ProgressDialog(context)
        progressLoading.window?.setBackgroundDrawableResource(android.R.color.transparent)
        binding?.rvDarah?.layoutManager = ln
        binding?.rvDarah?.adapter = darahAdapter
//
//        initLoad()
        return binding?.root
    }


//    private fun initLoad() {
//        progressLoading.show()
//        progressLoading.setContentView(R.layout.dialog_loading)
//        infoViewModel.getDarah()
//        infoViewModel.darah.observe(viewLifecycleOwner, Observer { data ->
//            if(data !== null) {
//                data.forEach {
//                    addDarahList.add(
//                        DarahView(it.id_darah.toString(),it.nama_darah,it.komponen)
//                    )
//                }
////                darahAdapter.setDarah(addDarahList)
//            }
//            progressLoading.dismiss()
//        })
//    }


}