package com.rsdunda.layananmandiri.ui.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.rsdunda.layananmandiri.data.model.darah.Darah
import com.rsdunda.layananmandiri.data.model.darah.Komponen
import com.rsdunda.layananmandiri.data.network.CloudApi
import kotlinx.coroutines.launch
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class DarahViewModel : ViewModel() {
    private var darah = MutableLiveData<List<Darah>?>()
    private var komponen = MutableLiveData<List<Komponen>>()
    var isLoading = MutableLiveData<Boolean>()

    init {
        darah.postValue(mutableListOf())
        komponen.postValue(mutableListOf())
        isLoading.value = false
    }
    
    
    fun getDarah() {
        viewModelScope.launch {
            onLoading()
            CloudApi().getDarah().enqueue(object : Callback<List<Darah>>{
                override fun onFailure(call: Call<List<Darah>>, t: Throwable) {
                    darah.postValue(null)
                    offLoading()
                }

                override fun onResponse(call: Call<List<Darah>>, response: Response<List<Darah>>) {
                    if(response.isSuccessful) {
                        darah.postValue(response.body())
                        offLoading()
                    }
                }

            })
        }
    }

    fun setKomponen(data: List<Komponen>) {
        println("DATA ===> $data")
        komponen.postValue(data)
    }

    private fun onLoading(){
        isLoading.value = true
    }

    private fun offLoading() {
        isLoading.value = false
    }


    fun listenDarah() = darah
    fun listenKomponent() = komponen

}