package com.rsdunda.layananmandiri.ui.viewmodel.fragments.Info

import android.app.ProgressDialog
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.rsdunda.layananmandiri.R
import com.rsdunda.layananmandiri.databinding.FragmentInfoBinding
import com.rsdunda.layananmandiri.ui.adapter.StrukAdapter
import com.rsdunda.layananmandiri.ui.viewmodel.InfoFragmentViewModel
import com.rsdunda.layananmandiri.util.SharedUsers


class InfoFragment : Fragment() {
    private var _binding: FragmentInfoBinding? = null
    private val binding get() = _binding
    private lateinit var sharedUsers: SharedUsers
    private lateinit var infoFragmentViewModel: InfoFragmentViewModel
    private lateinit var strukAdapter: StrukAdapter
    private lateinit var progressLoading: ProgressDialog
    override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        _binding = FragmentInfoBinding.inflate(layoutInflater, container, false)
        sharedUsers = SharedUsers(requireContext())
        infoFragmentViewModel = ViewModelProvider(requireActivity()).get(InfoFragmentViewModel::class.java)
        strukAdapter = StrukAdapter(mutableListOf(), requireContext(), infoFragmentViewModel)
        progressLoading = ProgressDialog(context)
        progressLoading.window?.setBackgroundDrawableResource(android.R.color.transparent)


        binding?.rvStrukPendaftaran?.apply {
            layoutManager = LinearLayoutManager(requireContext())
            adapter = strukAdapter
        }

        binding?.swipeInfo?.setOnRefreshListener {
            initLoad()
        }

        initLoad()
        return binding?.root
    }

    private fun initLoad() {
        binding?.swipeInfo?.isRefreshing = true
        infoFragmentViewModel.getStukPendaftaran(sharedUsers.session!!)
        infoFragmentViewModel.isLoading.observe(viewLifecycleOwner, Observer {
            if(it) {
                progressLoading.show()
                progressLoading.setContentView(R.layout.dialog_loading)
            } else {
                progressLoading.dismiss()
            }
        })

        infoFragmentViewModel.listenStruk().observe(viewLifecycleOwner, Observer {
            if(it != null) {
                binding?.rvStrukPendaftaran?.adapter?.let { a ->
                    if(a is StrukAdapter) {
                        a.updateLis(it)
                    }
                }
            }

            binding?.swipeInfo?.isRefreshing = false
        })

    }

}