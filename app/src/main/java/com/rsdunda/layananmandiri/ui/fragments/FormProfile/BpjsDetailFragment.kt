package com.rsdunda.layananmandiri.ui.fragments.FormProfile

import android.app.ProgressDialog
import android.content.DialogInterface
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.asLiveData
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.observe
import androidx.navigation.fragment.findNavController
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.rsdunda.layananmandiri.MainActivity
import com.rsdunda.layananmandiri.R
import com.rsdunda.layananmandiri.data.model.UserRequest
import com.rsdunda.layananmandiri.databinding.FragmentBpjsDetailBinding
import com.rsdunda.layananmandiri.ui.auth.AuthViewModel
import com.rsdunda.layananmandiri.util.SharedUsers
import com.rsdunda.layananmandiri.util.UserManager
import kotlinx.coroutines.launch


class BpjsDetailFragment : BottomSheetDialogFragment() {
    private var _binding: FragmentBpjsDetailBinding? = null
    private val binding get() = _binding
    private val authViewModel: AuthViewModel by activityViewModels()
    private lateinit var progressLoading: ProgressDialog
    private lateinit var sharedUsers: SharedUsers
    private lateinit var userManager: UserManager
    var session = ""
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        _binding = FragmentBpjsDetailBinding.inflate(layoutInflater, container, false)
        sharedUsers = SharedUsers(requireContext())
        userManager = UserManager(requireContext())
        userManager.session.asLiveData().observe(viewLifecycleOwner) {
            session = it
        }
        progressLoading = ProgressDialog(context)
        progressLoading.window?.setBackgroundDrawableResource(android.R.color.transparent)

        authViewModel.dataBpjs.observe(viewLifecycleOwner, Observer {
            if(it !== null) {
                binding?.nomorKartu?.text = it.response.peserta.noKartu
                binding?.nama?.text = it.response.peserta.nama
                binding?.nik?.text = it.response.peserta.nik
                binding?.tanggalLahir?.text = it.response.peserta.tglLahir
                binding?.status?.text = it.response.peserta.statusPeserta.keterangan
            }
        })


        binding?.btnNext?.setOnClickListener {
//            Toast.makeText(requireContext(), sharedUsers.session.toString(), Toast.LENGTH_SHORT).show()
            progressLoading.show()
            progressLoading.setContentView(R.layout.dialog_loading)
//            var user: UserRequest
            authViewModel.let {
                var user = UserRequest(
                        nik = it.nik.value.toString(),
                        norm = if(it.norm.value.isNullOrEmpty()) "" else it.norm.value.toString(),
                        nobpjs = binding?.nomorKartu?.text.toString(),
                        nama_lengkap = it.nama_lengkap.value.toString(),
                        kelamin = it.kelamin.value.toString(),
                        tempat_lahir = it.tempat_lahir.value.toString(),
                        tanggal_lahir = it.tanggal_lahir.value.toString(),
                        alamat =  it.alamat.value.toString(),
                        rt = it.rt.value.toString(),
                        rw = it.rw.value.toString(),
                        kelurahan = if(it.kelurahan.value.isNullOrEmpty()) "" else it.kelurahan.value.toString(),
                        kecamatan = if(it.kecamatan.value.isNullOrEmpty()) "" else it.kecamatan.value.toString(),
                        foto = if(it.foto.value.isNullOrEmpty()) "" else it.foto.value.toString(),
                        session = session
                )
                it.registerUser(user)
            }
            authViewModel.regisInfo.observe(viewLifecycleOwner, Observer {
                if(it !== null && it == true) {
                    progressLoading.dismiss()
                    lifecycleScope.launch {
                        authViewModel.let {
                            userManager.storeUsers(
                                no_hp = it.number.value.toString(),
                                nama = it.nama_lengkap.value.toString(),
                                norm = it.norm.value.toString(),
                                nobpjs = it.nobpjs.value.toString(),
                                alamat = it.alamat.value.toString(),
                                session = session,
                                auth = true,
                                foto = it.foto.value.toString(),
                                role = it.role.value.toString(),
                                nik = it.nik.value.toString(),
                                kelamin = it.kelamin.value.toString()
                            )

                            sharedUsers.let { user ->
                                user.nomorhp = it.number.value.toString()
                                user.nama_lengkap = it.nama_lengkap.value.toString()
                                user.no_bpjs = it.nobpjs.value.toString()
                                user.alamat = it.alamat.value.toString()
                                user.session = session
                                user.isAuth = true
                                user.foto = it.foto.value.toString()
                                user.role = it.role.value.toString()
                                user.nik = it.nik.value.toString()
                                user.kelamin = it.kelamin.value.toString()
                            }
                        }
                    }
                    sharedUsers.isAuth = true
                    activity?.finish()
                    startActivity(Intent(requireContext(), MainActivity::class.java))
                }

                if(it !== null && it == false) {
                    progressLoading.dismiss()
                    Toast.makeText(requireContext(), "Gagal simpan profil", Toast.LENGTH_SHORT).show()
                }
            })
        }

        return binding?.root
    }


    override fun onCancel(dialog: DialogInterface) {
        super.onCancel(dialog)
        findNavController().navigate(R.id.action_bpjsDetailFragment_to_bpjsFragment)
        authViewModel.resetDataBpjs()
    }




    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }


}