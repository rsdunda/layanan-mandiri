package com.rsdunda.layananmandiri.ui.fragments.Pendaftaran

import android.app.ProgressDialog
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.activity.OnBackPressedCallback
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import androidx.navigation.findNavController
import androidx.navigation.fragment.findNavController
import com.rsdunda.layananmandiri.R
import com.rsdunda.layananmandiri.databinding.FragmentBpjs2Binding
import com.rsdunda.layananmandiri.databinding.FragmentBpjsBinding
import com.rsdunda.layananmandiri.ui.auth.AuthViewModel

class BpjsFragment : Fragment() {
    private var _binding: FragmentBpjs2Binding? = null
    private lateinit var progressLoading: ProgressDialog
    private val binding get() = _binding
    private val authViewModel: AuthViewModel by activityViewModels()
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentBpjs2Binding.inflate(layoutInflater, container, false)
        progressLoading = ProgressDialog(context)
        progressLoading.window?.setBackgroundDrawableResource(android.R.color.transparent)


        binding?.buttonCari?.setOnClickListener {
            progressLoading.show()
            progressLoading.setContentView(R.layout.dialog_loading)
            authViewModel.checkBPJS(binding?.etNobpjs?.text.toString())
            authViewModel.dataBpjs.observe(viewLifecycleOwner, Observer {
                if(it != null) {
                    when(it.metadata.code.toInt()) {
                        200 -> {
                            findNavController().navigate(R.id.action_bpjsFragment2_to_detailBpjsFragment)
                            progressLoading.dismiss()
                        }

                        201 -> {

                            progressLoading.dismiss()
                        }
                    }
                }
            })
//            authViewModel.dataBpjs.observe(viewLifecycleOwner, Observer {
//                if (it != null) {
//                    when (it.metadata.code.toInt()) {
//                        200 -> {
//                            binding?.root?.findNavController()?.navigate(R.id.action_bpjsFragment_to_bpjsDetailFragment)
//                            progressLoading.dismiss()
//                        }
//
//                        201 -> {
//                            progressLoading.dismiss()
//                            Toast.makeText(requireContext(), it.metadata.message, Toast.LENGTH_SHORT).show()
//                            authViewModel.resetDataBpjs()
//                            activity?.recreate()
//                        }
//                    }
//                }
//            })
        }


        kembali()
        return binding?.root
    }





    fun kembali() {
        val callback = object : OnBackPressedCallback(true) {
            override fun handleOnBackPressed() {
//                findNavController().navigate(R.id.action_bpjsFragment_to_kembaliFragment)
            }
        }

        requireActivity().onBackPressedDispatcher.addCallback(callback)
    }


    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }
}