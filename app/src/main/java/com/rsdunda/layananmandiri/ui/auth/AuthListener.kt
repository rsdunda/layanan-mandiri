package com.rsdunda.layananmandiri.ui.auth

import androidx.lifecycle.LiveData

interface AuthListener {

    fun onStarted()
    fun onSuccess(message: String)
    fun onWarning(message: String)
    fun onFailure(message: String)
}