package com.rsdunda.layananmandiri.ui.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.rsdunda.layananmandiri.data.model.Pasien.ScanResponse
import com.rsdunda.layananmandiri.data.model.Pendaftaran.Data
import com.rsdunda.layananmandiri.data.network.CloudApi
import kotlinx.coroutines.launch
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class InfoFragmentViewModel: ViewModel() {
    private var struk = MutableLiveData<List<Data>>()
    var isLoading = MutableLiveData<Boolean>()
    init {
        struk.postValue(mutableListOf())
        isLoading.value = false
    }

    fun getStukPendaftaran(session: String) {
        viewModelScope.launch {
            setOnLoading()
            CloudApi().getPendaftar(session).enqueue(object: Callback<List<Data>>{
                override fun onFailure(call: Call<List<Data>>, t: Throwable) {
                    setOffLoading()
                }

                override fun onResponse(call: Call<List<Data>>, response: Response<List<Data>>) {
                    if(response.isSuccessful) {
                        struk.postValue(response.body())
                        setOffLoading()
                    }
                }

            })
        }
    }


    fun scanStrukPendaftaran(code: String) {
        viewModelScope.launch {
            setOnLoading()
            CloudApi().scanQrCode(code).enqueue(object : Callback<ScanResponse>{
                override fun onFailure(call: Call<ScanResponse>, t: Throwable) {
                    setOffLoading()
                }

                override fun onResponse(
                    call: Call<ScanResponse>,
                    response: Response<ScanResponse>
                ) {
                    if(response.isSuccessful) {
                        setOffLoading()
                    }
                }

            })
        }
    }

    fun listenStruk() = struk


    private fun setOffLoading() { isLoading.value = false }

    private fun setOnLoading() {isLoading.value = true}


}