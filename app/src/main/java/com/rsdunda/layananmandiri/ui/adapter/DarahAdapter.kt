package com.rsdunda.layananmandiri.ui.adapter

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.card.MaterialCardView
import com.google.gson.Gson
import com.rsdunda.layananmandiri.R
import com.rsdunda.layananmandiri.data.model.darah.Darah
import com.rsdunda.layananmandiri.data.model.darah.Komponen
import com.rsdunda.layananmandiri.data.model.darah.KomponenView
import com.rsdunda.layananmandiri.ui.activity.KomponenDarah
import com.rsdunda.layananmandiri.ui.viewmodel.DarahViewModel

class DarahAdapter(private var darah:MutableList<Darah>, private var context: Context, private var darahViewModel: DarahViewModel): RecyclerView.Adapter<DarahAdapter.ViewHolder> () {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.darah_items, parent,false))
    }

    override fun getItemCount() = darah.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) = holder.bind(darah[position], context, darahViewModel)

    fun updateList(data: List<Darah>) {
        darah.clear()
        darah.addAll(data)
        notifyDataSetChanged()
    }

    class ViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {
        val btn_darah = itemView.findViewById<MaterialCardView>(R.id.btn_darah)
        val img_darah = itemView.findViewById<ImageView>(R.id.img_darah)
        val arrList:MutableList<Komponen> = mutableListOf()
        fun bind(data:Darah, context: Context, darahViewModel: DarahViewModel) {
            val sharedPreferences: SharedPreferences = context.getSharedPreferences("shared preferences",
                Context.MODE_PRIVATE
            )
            val editor = sharedPreferences.edit()
            val gson = Gson()

            btn_darah.setOnClickListener {
                data.komponen.forEach {
                    arrList.add(Komponen(it.id_jenisdarah,it.nama_komponen,it.stok))
                }
                val json = gson.toJson(arrList)
                editor.putString("komponen", json);
                editor.apply()

                val intent = Intent(context, KomponenDarah::class.java)
//                editoritor.putString("komponen", json);
                intent.putExtra("nama_darah", data.nama_darah)
                context.startActivity(intent)
            }

            when(data.nama_darah) {
                "A+" -> img_darah.setImageResource(R.drawable.ic_ap)
                "B+" -> img_darah.setImageResource(R.drawable.ic_bp)
                "AB+" -> img_darah.setImageResource(R.drawable.ic_abp)
                "O+" -> img_darah.setImageResource(R.drawable.ic_op)
                "A-" -> img_darah.setImageResource(R.drawable.ic_am)
                "B-" -> img_darah.setImageResource(R.drawable.ic_bm)
                "AB-" -> img_darah.setImageResource(R.drawable.ic_abm)
                "O-" -> img_darah.setImageResource(R.drawable.ic_om)
            }
        }
    }


}


//inner class DarahViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {
//    val btn_darah = itemView.findViewById<MaterialCardView>(R.id.btn_darah)
//    val img_darah = itemView.findViewById<ImageView>(R.id.img_darah)
//    val arrList:MutableList<KomponenView> = mutableListOf()
//
//
//    fun bindmodel(
//        p: Darah,
//        context: Context,
//        darahViewModel: DarahViewModel
//    ) {
//        val sharedPreferences: SharedPreferences =
//            this@DarahAdapter.context.getSharedPreferences("shared preferences", Context.MODE_PRIVATE)
//        val editor = sharedPreferences.edit()
//        val gson = Gson()
//
//        btn_darah.setOnClickListener {
//            p.KOMPONEN.forEach {
//                arrList.add(KomponenView(it.id_jenisdarah.toString(),
//                    it.nama_komponen,
//                    it.stok.toString()))
//            }
//
//            val json = gson.toJson(arrList)
//            editor.putString("komponen", json);
//            editor.apply()
//
//
////                val intent = Intent(context, KomponenDarah::class.java)
////                intent.putExtra("nama_darah", p.NAMA_DARAH)
////                context.startActivity(intent)
//        }
//        when(p.NAMA_DARAH) {
//            "A+" -> img_darah.setImageResource(R.drawable.ic_ap)
//            "B+" -> img_darah.setImageResource(R.drawable.ic_bp)
//            "AB+" -> img_darah.setImageResource(R.drawable.ic_abp)
//            "O+" -> img_darah.setImageResource(R.drawable.ic_op)
//            "A-" -> img_darah.setImageResource(R.drawable.ic_am)
//            "B-" -> img_darah.setImageResource(R.drawable.ic_bm)
//            "AB-" -> img_darah.setImageResource(R.drawable.ic_abm)
//            "O-" -> img_darah.setImageResource(R.drawable.ic_om)
//        }
//    }
//}