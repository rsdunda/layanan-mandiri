package com.rsdunda.layananmandiri.ui.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.rsdunda.layananmandiri.data.model.kamar.Kamar
import com.rsdunda.layananmandiri.data.model.kamar.KamarResponse
import com.rsdunda.layananmandiri.data.network.CloudApi
import com.rsdunda.layananmandiri.data.network.ServiceApi
import kotlinx.coroutines.launch
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class BedViewModel: ViewModel() {
    private var bed = MutableLiveData<List<Kamar>>()
    var isLoading = MutableLiveData<Boolean>()

    init {
        bed.postValue(mutableListOf())
        isLoading.value = false
    }


    fun getDataBed() {
            setOnLoading()
            CloudApi().getKamar().enqueue(object : Callback<List<Kamar>>{
                override fun onFailure(call: Call<List<Kamar>>, t: Throwable) {
                    setOffLoading()
                }
                override fun onResponse(call: Call<List<Kamar>>, response: Response<List<Kamar>>) {
                    if(response.isSuccessful) {
                        println("DATA ====> ${response.body()}" )
                        bed.postValue(response.body())
                        setOffLoading()
                    }
                }
            })
    }

    fun listenKamar() = bed


    private fun setOnLoading(){
        isLoading.value = true
    }

    private fun  setOffLoading() {
        isLoading.value = false
    }


}