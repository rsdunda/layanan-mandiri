package com.rsdunda.layananmandiri.ui.splashscreen

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import androidx.lifecycle.Observer
import androidx.lifecycle.asLiveData
import androidx.lifecycle.observe
import com.rsdunda.layananmandiri.MainActivity
import com.rsdunda.layananmandiri.R
import com.rsdunda.layananmandiri.ui.auth.AuthActivity
import com.rsdunda.layananmandiri.ui.fragments.FormProfile.FormProfile
import com.rsdunda.layananmandiri.util.SharedUsers
import com.rsdunda.layananmandiri.util.UserManager
import com.rsdunda.layananmandiri.util.toast

class SplashScreen : AppCompatActivity() {
    private lateinit var i: Intent
    private lateinit var sharedUsers: SharedUsers
    private lateinit var userManager: UserManager
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash_screen)
        actionBar?.hide()
        supportActionBar?.hide()

        sharedUsers = SharedUsers(this)
        userManager = UserManager(this)
        if(sharedUsers.isAuth) {
            i = Intent(this@SplashScreen, MainActivity::class.java)
        } else {
            i = Intent(this@SplashScreen, AuthActivity::class.java)
        }

        Handler().postDelayed({
            finish()
            startActivity(i)
        }, 2000)
    }

    override fun onStart() {
        super.onStart()
    }
}