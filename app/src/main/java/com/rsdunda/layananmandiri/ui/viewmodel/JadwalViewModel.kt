package com.rsdunda.layananmandiri.ui.viewmodel

import android.content.Context
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.rsdunda.layananmandiri.data.model.jadwal.Hari
import com.rsdunda.layananmandiri.data.model.jadwal.Jadwal
import com.rsdunda.layananmandiri.data.network.CloudApi
import kotlinx.coroutines.launch
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class JadwalViewModel: ViewModel() {
    private var jadwal = MutableLiveData<List<Jadwal>>()
    var hari = MutableLiveData<List<Hari>>()

    var isLoading = MutableLiveData<Boolean>()
    init {
        jadwal.postValue(mutableListOf())
        hari.postValue(mutableListOf())
        isLoading.value = false
    }


    fun getDataJadwal() {
        viewModelScope.launch {
            setOnLoading()
            CloudApi().getJadwal().enqueue(object : Callback<List<Jadwal>>{
                override fun onFailure(call: Call<List<Jadwal>>, t: Throwable) {
                    setOffLoading()
                }
                override fun onResponse(call: Call<List<Jadwal>>, response: Response<List<Jadwal>>) {
                    if(response.isSuccessful) {
                        jadwal.postValue(response.body())
                        setOffLoading()
                    }
                }
            })
        }
    }

    fun setDataHari(data: MutableList<Hari>) {
        println("DATA ======> $data")
        hari.postValue(data)
    }

    fun listenJadwal() = jadwal
    fun listenHari() = hari

    private fun setOnLoading(){
        isLoading.value = true
    }

    private fun  setOffLoading() {
        isLoading.value = false
    }


}