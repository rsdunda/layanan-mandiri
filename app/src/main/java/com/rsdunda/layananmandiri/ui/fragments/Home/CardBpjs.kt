package com.rsdunda.layananmandiri.ui.fragments.Home

import android.app.ProgressDialog
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.OnBackPressedCallback
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import com.rsdunda.layananmandiri.R
import com.rsdunda.layananmandiri.databinding.FragmentCardBpjsBinding
import com.rsdunda.layananmandiri.databinding.FragmentDetailBpjsBinding
import com.rsdunda.layananmandiri.ui.auth.AuthViewModel


class CardBpjs : Fragment() {
    private var _binding: FragmentCardBpjsBinding? = null
    private lateinit var progressLoading: ProgressDialog
    private val binding get() = _binding
    private val authViewModel: AuthViewModel by activityViewModels()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        _binding = FragmentCardBpjsBinding.inflate(inflater, container, false)

        authViewModel.dataBpjs.observe(viewLifecycleOwner, Observer {
            if(it !== null) {
                binding?.nomorKartu?.text = it.response.peserta.noKartu
                binding?.nama?.text = it.response.peserta.nama
                binding?.nik?.text = it.response.peserta.nik
                binding?.tanggalLahir?.text = it.response.peserta.tglLahir
                binding?.status?.text = it.response.peserta.statusPeserta.keterangan
            }
        })

        kembali()

        return binding?.root
    }


    fun kembali() {
        val callback = object : OnBackPressedCallback(true) {
            override fun handleOnBackPressed() {
                findNavController().navigate(R.id.action_cardBpjs_to_homeFragment)
            }
        }

        requireActivity().onBackPressedDispatcher.addCallback(callback)
    }


    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }

}