package com.rsdunda.layananmandiri.ui.fragments.Pendaftaran

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.app.Activity.RESULT_OK
import android.app.ProgressDialog
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.activity.OnBackPressedCallback
import androidx.core.app.ActivityCompat
import androidx.core.content.FileProvider
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.asLiveData
import androidx.lifecycle.observe
import androidx.navigation.fragment.findNavController
import com.google.android.gms.tasks.OnSuccessListener
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.StorageReference
import com.google.firebase.storage.UploadTask
import com.rsdunda.layananmandiri.MainActivity
import com.rsdunda.layananmandiri.R
import com.rsdunda.layananmandiri.databinding.FragmentStep2Binding
import com.rsdunda.layananmandiri.ui.viewmodel.PendaftaranViewModel
import com.rsdunda.layananmandiri.util.SharedUsers
import com.rsdunda.layananmandiri.util.UserManager
import java.io.ByteArrayOutputStream
import java.io.File
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.*


@Suppress("IMPLICIT_BOXING_IN_IDENTITY_EQUALS")
class Step2Fragment : Fragment() {
    private var _binding: FragmentStep2Binding? = null
    private val binding get() = _binding
    val REQUEST_IMAGE_CAPTURE = 2
    val REQUEST_TAKE_PHOTO = 1
    lateinit var storage:FirebaseStorage
    lateinit var storageRef: StorageReference
    lateinit var currentPhotoPath: String
    lateinit var userManager: UserManager
    lateinit var sharedUsers: SharedUsers
    private lateinit var progressLoading: ProgressDialog
    var BPJS = ""
    var STATUS_PENDAFTAR = ""
    private val pendaftaranViewModel: PendaftaranViewModel by activityViewModels()


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        _binding = FragmentStep2Binding.inflate(inflater,container, false)
        storage = FirebaseStorage.getInstance()
        storageRef = storage.reference
        kembali()

        STATUS_PENDAFTAR = activity?.intent?.extras?.getString("PENDAFTAR").toString()
        sharedUsers = SharedUsers(requireContext())
        userManager = UserManager(requireContext())
        userManager.no_bpjs.asLiveData().observe(viewLifecycleOwner) {
            if(it.isNotEmpty()) {
                BPJS = it
            }
        }

        // progres dialog
        progressLoading = ProgressDialog(context)
        progressLoading.window?.setBackgroundDrawableResource(android.R.color.transparent)



        binding?.btnMulai?.setOnClickListener {
            openCamera()
        }


        return binding?.root
    }



    @SuppressLint("QueryPermissionsNeeded")
    private fun openCamera() {
        Intent(MediaStore.ACTION_IMAGE_CAPTURE).also { takePictureIntent ->
            takePictureIntent.resolveActivity(requireContext().packageManager)?.also {
                progressLoading.show()
                progressLoading.setContentView(R.layout.dialog_loading)
                startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE)

            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if(resultCode == RESULT_OK) {
            if(requestCode == REQUEST_IMAGE_CAPTURE) {
                val randomKey = UUID.randomUUID().toString()
                val baos = ByteArrayOutputStream()
                var imageBitmap = data?.extras?.get("data") as Bitmap
                imageBitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos)
                val data = baos.toByteArray()
                val NAMA = activity?.intent?.extras?.getString("NAMA")
                val NORM = activity?.intent?.extras?.getString("NORM")
                val PENDAFTAR = activity?.intent?.extras?.getString("PENDAFTAR")

                val photoRef = storageRef.child("KTP/${NAMA}_${NORM}_$randomKey")
                photoRef.putBytes(data).addOnSuccessListener {
                    it.metadata?.name?.let { it1 -> pendaftaranViewModel.setKtp(it1) }
                    findNavController().navigate(R.id.action_step2Fragment_to_kepesertaanDialogFragment)
//                    when(PENDAFTAR) {
//                        "SENDIRI" -> {
//                            if(sharedUsers.no_bpjs !== "") {
//                                pendaftaranViewModel.setBPJS(sharedUsers.no_bpjs.toString())
//                                findNavController().navigate(R.id.action_step2Fragment_to_step3Fragment)
//                            } else {
//                                findNavController().navigate(R.id.action_step2Fragment_to_kepesertaanDialogFragment)
//                            }
//                        }
//                        "ORANG" -> findNavController().navigate(R.id.action_step2Fragment_to_kepesertaanDialogFragment)
//                    }
                    progressLoading.dismiss()
                }.addOnFailureListener {
                    Toast.makeText(requireContext(), it.message.toString(), Toast.LENGTH_SHORT).show()
                }
            }
        }
    }


    @SuppressLint("SimpleDateFormat")
    @Throws(IOException::class)
    private fun createImageFile(): File {
        // Create an image file name
        val timeStamp: String = SimpleDateFormat("yyyyMMdd_HHmmss").format(Date())
//        val storageDir: File =
        val imagePath = File(context?.filesDir, "images")
        val newFile = File(imagePath, "default_image.jpg")
        return File.createTempFile(
                "JPEG_${timeStamp}_", /* prefix */
                ".jpg", /* suffix */
                newFile /* directory */
        ).apply {
            // Save a file: path for use with ACTION_VIEW intents
            currentPhotoPath = absolutePath
        }
    }

    @SuppressLint("QueryPermissionsNeeded")
    private fun dispatchTakePictureIntent() {
        Intent(MediaStore.ACTION_IMAGE_CAPTURE).also { takePictureIntent ->
            // Ensure that there's a camera activity to handle the intent
            requireActivity().packageManager.let {
                takePictureIntent.resolveActivity(it)?.also {
                    // Create the File where the photo should go
                    val photoFile: File? = try {
                        createImageFile()
                    } catch (ex: IOException) {
                        // Error occurred while creating the File
                        null
                    }
                    // Continue only if the File was successfully created
                    photoFile?.also {
                        val photoURI: Uri = FileProvider.getUriForFile(
                                requireContext(),
                                "com.example.android.fileprovider",
                                it
                        )
                        takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI)
                        startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE)
                    }
                }
            }
        }
    }



    fun kembali() {
        val callback = object : OnBackPressedCallback(true) {
            override fun handleOnBackPressed() {
                startActivity(Intent(requireContext(), MainActivity::class.java))
                activity?.finish()
                onDestroy()
            }
        }

        requireActivity().onBackPressedDispatcher.addCallback(callback)
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }

    override fun onStart() {
        super.onStart()
        if(ActivityCompat.checkSelfPermission(requireContext(), Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(requireContext() as Activity, arrayOf(Manifest.permission.CAMERA), REQUEST_TAKE_PHOTO)
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if(requestCode == REQUEST_TAKE_PHOTO && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

        }
    }



}