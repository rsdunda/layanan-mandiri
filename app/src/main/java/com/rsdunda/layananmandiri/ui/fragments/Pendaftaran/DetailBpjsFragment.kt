package com.rsdunda.layananmandiri.ui.fragments.Pendaftaran

import android.app.ProgressDialog
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import com.rsdunda.layananmandiri.R
import com.rsdunda.layananmandiri.databinding.FragmentBpjs2Binding
import com.rsdunda.layananmandiri.databinding.FragmentDetailBpjsBinding
import com.rsdunda.layananmandiri.ui.auth.AuthViewModel


class DetailBpjsFragment : Fragment() {
    private var _binding: FragmentDetailBpjsBinding? = null
    private lateinit var progressLoading: ProgressDialog
    private val binding get() = _binding
    private val authViewModel: AuthViewModel by activityViewModels()

    var STATUS = ""

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        _binding = FragmentDetailBpjsBinding.inflate(layoutInflater, container, false)

        authViewModel.dataBpjs.observe(viewLifecycleOwner, Observer {
            if(it !== null) {
                binding?.nomorKartu?.text = it.response.peserta.noKartu
                binding?.nama?.text = it.response.peserta.nama
                binding?.nik?.text = it.response.peserta.nik
                binding?.tanggalLahir?.text = it.response.peserta.tglLahir
                binding?.status?.text = it.response.peserta.statusPeserta.keterangan
                STATUS = it.response.peserta.statusPeserta.keterangan
            }
        })

        binding?.btnNext?.setOnClickListener {
            if(STATUS == "AKTIF") {
                findNavController().navigate(R.id.action_detailBpjsFragment_to_step3Fragment)
            } else {
                Toast.makeText(requireContext(), "Status anda NONAKTIF", Toast.LENGTH_SHORT).show()
            }
        }
        return binding?.root
    }

}