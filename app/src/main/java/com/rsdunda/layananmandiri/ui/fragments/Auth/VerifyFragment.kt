package com.rsdunda.layananmandiri.ui.fragments.Auth

import android.annotation.SuppressLint
import android.app.Activity
import android.app.ProgressDialog
import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.observe
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.auth.PhoneAuthCredential
import com.google.firebase.auth.PhoneAuthProvider
import com.rsdunda.layananmandiri.MainActivity
import com.rsdunda.layananmandiri.R
import com.rsdunda.layananmandiri.databinding.FragmentVerifyBinding
import com.rsdunda.layananmandiri.ui.auth.AuthActivity
import com.rsdunda.layananmandiri.ui.auth.AuthViewModel
import com.rsdunda.layananmandiri.ui.fragments.FormProfile.FormProfile
import com.rsdunda.layananmandiri.util.SharedUsers
import com.rsdunda.layananmandiri.util.UserManager
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

@Suppress("DEPRECATION")
class VerifyFragment : Fragment() {
    private var _binding: FragmentVerifyBinding? = null
    private lateinit var auth: FirebaseAuth
    private lateinit var progressLoading: ProgressDialog
    private val binding get() = _binding
    private val authViewModel: AuthViewModel by activityViewModels()
    private lateinit var userManager: UserManager
    private lateinit var sharedUsers: SharedUsers
    private lateinit var userCredential: FirebaseUser
    private var sessionId: String? = ""


    @SuppressLint("SetTextI18n")
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        _binding = FragmentVerifyBinding.inflate(inflater, container, false)
//        sharedUsers = SharedUsers(requireContext())
        userManager = UserManager(requireContext())
        sharedUsers = SharedUsers(requireContext())
        progressLoading = ProgressDialog(context)
        progressLoading.window?.setBackgroundDrawableResource(android.R.color.transparent)

        auth = FirebaseAuth.getInstance()
        authViewModel.number.observe(viewLifecycleOwner) {
            binding?.textNomorHp?.text = "+62 $it"
        }



        binding?.textNomorHp

        binding?.btnVerifikasi?.setOnClickListener {
            authViewModel.verifyId.observe(viewLifecycleOwner) {
                progressLoading.show()
                progressLoading.setContentView(R.layout.dialog_loading)
                val credential = PhoneAuthProvider.getCredential(it, binding?.code?.text.toString())
                signInWithPhoneAuthCredential(credential)
            }
        }

        return binding?.root
    }


    private fun signInWithPhoneAuthCredential(credential: PhoneAuthCredential) {
        auth.signInWithCredential(credential).addOnCompleteListener {
            if (it.isSuccessful) {
                userCredential = it.result?.user!!
                sharedUsers.session = userCredential.uid
                lifecycleScope.launch {
//                    user?.uid?.let { it1 -> sharedUsers.setSessions(it1) }
                    userManager.setSession(uid = userCredential.uid)
                }
                sessionId = userCredential.uid
                userCredential.uid.let { it1 ->
                    authViewModel.setSession(it1)
                    checkUser(it1)
                }


                progressLoading.setContentView(R.layout.dialog_success)
                progressLoading.dismiss()
            } else {
                progressLoading.dismiss()
            }
        }
    }

    private fun checkUser(uid: String) {
        authViewModel.checkUsers(uid)
        authViewModel.users.observe(viewLifecycleOwner, Observer {
            if(it !== null) {
                sharedUsers.let { user ->
                    user.nik = it.data.nik
                    user.norm = it.data.norm
                    user.no_bpjs = it.data.nobpjs
                    user.nama_lengkap = it.data.nama_lengkap
                    user.kelamin = it.data.kelamin
                    user.alamat = it.data.alamat
                    user.nomorhp = binding?.textNomorHp?.text.toString()
                    user.foto = it.data.foto
                    user.role = it.data.role
                    user.session = uid
                    user.isAuth = true
                }
                startActivity(Intent(requireContext(), MainActivity::class.java))
            } else {
                val i = Intent(requireContext(), FormProfile::class.java)
                sharedUsers.session = uid
                sharedUsers.nomorhp = binding?.textNomorHp?.text.toString()
                i.putExtra("SESSIONID", sessionId)
                activity?.finish()
                startActivity(i)
            }
        })
//        if (true) {
//            authViewModel.users.observe(viewLifecycleOwner) {
//
//                lifecycleScope.launch {
//                    userManager.storeUsers(it.nik, it.norm, it.nobpjs, it.nama_lengkap, it.kelamin, it.alamat, binding?.textNomorHp?.text.toString(),it.foto, it.role, true, uid)
//                }
//            }
//            startActivity(Intent(requireContext(), MainActivity::class.java))
//        } else {
//            val i = Intent(requireContext(), FormProfile::class.java)
//            i.putExtra("SESSIONID", sessionId)
//            requireActivity().finish()
//            startActivity(i)
//        }
    }


    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

}