package com.rsdunda.layananmandiri.ui.fragments.Home

import android.app.ProgressDialog
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.OnBackPressedCallback
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import com.rsdunda.layananmandiri.R
import com.rsdunda.layananmandiri.databinding.FragmentCheckBpjsBinding
import com.rsdunda.layananmandiri.databinding.FragmentDarahBinding
import com.rsdunda.layananmandiri.ui.auth.AuthViewModel


class CheckBpjs : Fragment() {
    private var _binding: FragmentCheckBpjsBinding? = null
    private val binding get() = _binding
    private lateinit var progressLoading: ProgressDialog
    private val authViewModel: AuthViewModel by activityViewModels()
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        _binding = FragmentCheckBpjsBinding.inflate(inflater, container, false)
        progressLoading = ProgressDialog(context)
        progressLoading.window?.setBackgroundDrawableResource(android.R.color.transparent)


        binding?.buttonCari?.setOnClickListener {
            progressLoading.show()
            progressLoading.setContentView(R.layout.dialog_loading)
            authViewModel.checkBPJS(binding?.etNobpjs?.text.toString())
            authViewModel.dataBpjs.observe(viewLifecycleOwner, Observer {
                if(it != null) {
                    when(it.metadata.code.toInt()) {
                        200 -> {
                            findNavController().navigate(R.id.action_checkBpjs_to_cardBpjs)
                            progressLoading.dismiss()
                        }

                        201 -> {

                            progressLoading.dismiss()
                        }
                    }
                }
            })

        }


        kembali()

        return binding?.root

    }


    fun kembali() {
        val callback = object : OnBackPressedCallback(true) {
            override fun handleOnBackPressed() {
                findNavController().navigate(R.id.action_checkBpjs_to_homeFragment)
            }
        }

        requireActivity().onBackPressedDispatcher.addCallback(callback)
    }


    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }


}