package com.rsdunda.layananmandiri.ui.activity

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.rsdunda.layananmandiri.R
import com.rsdunda.layananmandiri.data.model.jadwal.Hari
import com.rsdunda.layananmandiri.ui.adapter.JadwalHariAdapter
import com.rsdunda.layananmandiri.ui.viewmodel.JadwalViewModel
import org.json.JSONArray

class JadwalHari : AppCompatActivity() {
    private lateinit var jadwalViewModel: JadwalViewModel
    private lateinit var rv_hari: RecyclerView
    private lateinit var jadwalHariAdapter: JadwalHariAdapter
    val addHariList: MutableList<Hari> = mutableListOf()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_jadwal_hari)
        val extras = intent.extras
        supportActionBar?.title = "Jadwal Poli ${extras?.getString("nama_poli")}"
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setHomeButtonEnabled(true)
        actionBar?.title = "Jadwal Poli ${extras?.getString("nama_poli")}"

        jadwalViewModel = ViewModelProvider(this).get(JadwalViewModel::class.java)
        jadwalHariAdapter = JadwalHariAdapter(mutableListOf(), this, jadwalViewModel)
        rv_hari = findViewById(R.id.rv_hari)
        rv_hari.layoutManager = LinearLayoutManager(this@JadwalHari)

        val sharedPreferences = getSharedPreferences("shared preferences", MODE_PRIVATE)
        val json = sharedPreferences.getString("jadwal", null)

        val jsonArr = JSONArray(json)
        for (i in 0 until jsonArr.length()) {
            val jsonData = jsonArr.getJSONObject(i)
            addHariList.add(Hari(
                jsonData.getInt("_id"),
                jsonData.getString("dokter"),
                jsonData.getInt("id_dokter"),
                jsonData.getString("nama")
                ))
        }

        jadwalHariAdapter.updateList(addHariList)
        rv_hari.adapter = jadwalHariAdapter

    }


    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        super.onOptionsItemSelected(item)
        when(item.itemId) {
            android.R.id.home -> {
                onBackPressed()
            }
        }
        return true
    }

    override fun onBackPressed() {
        super.onBackPressed()
        finish()
        val intent = Intent(this@JadwalHari, JadwalDokter::class.java)
        startActivity(intent)

    }
}