package com.rsdunda.layananmandiri.ui.viewmodel.fragments.Profil

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.rsdunda.layananmandiri.R
import com.rsdunda.layananmandiri.databinding.FragmentProfileBinding
import com.rsdunda.layananmandiri.databinding.FragmentStep1Binding
import com.rsdunda.layananmandiri.ui.splashscreen.SplashScreen
import com.rsdunda.layananmandiri.util.SharedUsers


class ProfileFragment : Fragment() {
    private var _binding: FragmentProfileBinding? = null
    private val binding get() = _binding
    private lateinit var sharedUsers: SharedUsers
    @SuppressLint("SetTextI18n")
    override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentProfileBinding.inflate(inflater, container, false)
        sharedUsers = SharedUsers(requireContext())

        sharedUsers.let {
            binding?.textNama?.text =  it.nama_lengkap
            binding?.textNik?.text = "NIK : ${it.nik}"
            binding?.textNomorHp?.text = "Nomor HP ${it.nomorhp}"
        }

        binding?.btnLogout?.setOnClickListener {
            sharedUsers.isAuth = false
            startActivity(Intent(requireContext(), SplashScreen::class.java))
            activity?.finish()
        }

        return binding?.root
    }


}