package com.rsdunda.layananmandiri.ui.adapter

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.card.MaterialCardView
import com.rsdunda.layananmandiri.R
import com.rsdunda.layananmandiri.data.model.Pendaftaran.Data
import com.rsdunda.layananmandiri.ui.fragments.Pendaftaran.StrukPendaftaran
import com.rsdunda.layananmandiri.ui.viewmodel.InfoFragmentViewModel
import java.text.SimpleDateFormat
import java.util.*

@Suppress("NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS")
class StrukAdapter(
    private var struk:MutableList<Data>,
    private var context: Context,
    private var infoFragmentViewModel: InfoFragmentViewModel
):RecyclerView.Adapter<StrukAdapter.ViewHolder>() {


    class ViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {
        @SuppressLint("SimpleDateFormat")
        val formatDate = SimpleDateFormat("dd/MM/yyyy", Locale.ROOT)

        val btn_struk = itemView.findViewById<MaterialCardView>(R.id.btn_struk)
        val nama_pasien = itemView.findViewById<TextView>(R.id.nama_pasien)
        val text_norm = itemView.findViewById<TextView>(R.id.text_norm)
        val poli_tujuan = itemView.findViewById<TextView>(R.id.poli_tujuan)
        val text_tanggal = itemView.findViewById<TextView>(R.id.text_tanggal)
        val pending = itemView.findViewById<CardView>(R.id.cardStatusPending)
        val diterima = itemView.findViewById<CardView>(R.id.cardStatusDiterima)
        val ditolak = itemView.findViewById<CardView>(R.id.cardStatusDitolak)
        @SuppressLint("SimpleDateFormat")
        fun bind(data: Data, context: Context, infoFragmentViewModel: InfoFragmentViewModel) {
            val parse = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss")

            nama_pasien.text = data.NAMA
            text_norm.text = data.NORM
            poli_tujuan.text = data.POLI
            text_tanggal.text = formatDate.format(parse.parse(data.TGL_BOOKING))

            when(data.STATUS) {
                "0" -> {
                    pending.visibility = View.VISIBLE
                    diterima.visibility = View.GONE
                    ditolak.visibility = View.GONE
                }
                "1" -> {
                    pending.visibility = View.GONE
                    diterima.visibility = View.VISIBLE
                    ditolak.visibility = View.GONE
                }
                "2" -> {
                    pending.visibility = View.GONE
                    diterima.visibility = View.GONE
                    ditolak.visibility = View.VISIBLE
                }

            }


            btn_struk.setOnClickListener {
                val i = Intent(context, StrukPendaftaran::class.java)
                i.putExtra("NO_REGIS", data.NO_REGIS)
                i.putExtra("TGL_BOOKING", formatDate.format(parse.parse(data.TGL_BOOKING)))
                i.putExtra("NORM", data.NORM)
                i.putExtra("NIK", data.NIK)
                i.putExtra("NAMA", data.NAMA)
                i.putExtra("POLI", data.POLI)
                i.putExtra("KEPERLUAN", data.KEPERLUAN)
                i.putExtra("PESAN", data.PESAN)
                i.putExtra("KEPESERTAAN", data.KEPESERTAAN)
                i.putExtra("NO_SEP", data.NO_SEP)
                i.putExtra("STATUS", data.STATUS)
                context.startActivity(i)
            }
        }
    }

    fun updateLis(data: List<Data>) {
        struk.clear()
        struk.addAll(data)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_pendaftaran, parent, false))
    }

    override fun getItemCount() = struk.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) = holder.bind(struk[position], context, infoFragmentViewModel)
}