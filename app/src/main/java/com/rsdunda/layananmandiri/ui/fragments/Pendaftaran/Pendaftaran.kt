package com.rsdunda.layananmandiri.ui.fragments.Pendaftaran

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.rsdunda.layananmandiri.R
import com.rsdunda.layananmandiri.databinding.ActivityPendaftaranBinding

class Pendaftaran : AppCompatActivity() {
    private lateinit var binding: ActivityPendaftaranBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityPendaftaranBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)
        actionBar?.hide()
        supportActionBar?.hide()

    }
}