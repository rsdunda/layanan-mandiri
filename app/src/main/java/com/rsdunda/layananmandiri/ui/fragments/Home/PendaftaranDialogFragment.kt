package com.rsdunda.layananmandiri.ui.fragments.Home

import android.annotation.SuppressLint
import android.app.AlertDialog
import android.app.Dialog
import android.app.ProgressDialog
import android.content.Intent
import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.InsetDrawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.asLiveData
import androidx.navigation.fragment.findNavController
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.google.android.material.button.MaterialButton
import com.rsdunda.layananmandiri.R
import com.rsdunda.layananmandiri.databinding.FragmentPendaftaranDialogBinding
import com.rsdunda.layananmandiri.ui.fragments.Pendaftaran.Pendaftaran
import com.rsdunda.layananmandiri.ui.viewmodel.PendaftaranViewModel
import com.rsdunda.layananmandiri.util.SharedUsers
import com.rsdunda.layananmandiri.util.UserManager

class PendaftaranDialogFragment : BottomSheetDialogFragment() {
    private var _binding: FragmentPendaftaranDialogBinding? = null
    private val binding get() = _binding
//    private lateinit var formDialog: View
    private lateinit var sharedUsers: SharedUsers
    private lateinit var userManager: UserManager
    private lateinit var progressLoading: ProgressDialog
    private val pendaftaranViewModel: PendaftaranViewModel by activityViewModels()


    @SuppressLint("SetTextI18n", "ResourceAsColor")
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        _binding = FragmentPendaftaranDialogBinding.inflate(inflater, container, false)
        sharedUsers = SharedUsers(requireContext())
        userManager = UserManager(requireContext())

//        formDialog = LayoutInflater.from(context).inflate(R.layout.modal_norm, null)
        progressLoading = ProgressDialog(context)
        progressLoading.window?.setBackgroundDrawableResource(android.R.color.transparent)

//        Toast.makeText(requireContext(), sharedUsers.norm, Toast.LENGTH_SHORT).show()
        println("DATA ==> ${sharedUsers.nama_lengkap}")
        println("DATA ==> ${sharedUsers.alamat}")
        println("DATA ==> ${sharedUsers.kelamin}")
        println("DATA ==> ${sharedUsers.isAuth}")
        println("DATA ==> ${sharedUsers.norm}")

        binding?.btnSayaSendiri?.setOnClickListener {
            if(sharedUsers.norm.isNullOrEmpty()) {
                val dialog = Dialog(requireContext())
                dialog.setContentView(R.layout.modal_norm)
                dialog.window?.setBackgroundDrawable(InsetDrawable(activity?.getDrawable(R.drawable.background_modal), 20))
                dialog.window?.setLayout(
                    ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT
                )
                dialog.setCancelable(true)
                dialog.show()
//                val dialogBuild = context?.let { db -> AlertDialog.Builder(db) }
//                dialogBuild?.setView(formDialog)
//                checkInputNORM()
//                val mAlerDialog = dialogBuild?.show()
//                mAlerDialog?.window?.setBackgroundDrawable(ColorDrawable(android.R.color.transparent))
                dialog.findViewById<MaterialButton>(R.id.btn_cek_norm).setOnClickListener {
                    val error = dialog.findViewById<TextView>(R.id.text_error)
                    val norm = dialog.findViewById<EditText>(R.id.et_norm)
                    if(norm.text.toString().trim().isEmpty()) {
                        error.text = "NORM masih kosong"
                    } else {
                        dialog.dismiss()
                        checkNORM(norm.text.toString(), "SENDIRI")
                    }
                }
            } else {
                checkNORM(sharedUsers.norm.toString(), "SENDIRI")
            }
        }



        binding?.btnOrangLain?.setOnClickListener {
            val dialog = Dialog(requireContext())
            dialog.setContentView(R.layout.modal_norm)
            dialog.window?.setBackgroundDrawable(InsetDrawable(activity?.getDrawable(R.drawable.background_modal), 20))
            dialog.window?.setLayout(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT
            )
            dialog.setCancelable(true)
            dialog.show()
            dialog.findViewById<MaterialButton>(R.id.btn_cek_norm).setOnClickListener {
                val error = dialog.findViewById<TextView>(R.id.text_error)
                val norm = dialog.findViewById<EditText>(R.id.et_norm)
                if(norm.text.toString().trim().isEmpty()) {
                    error.text = "NORM masih kosong"
                } else {
                    dialog?.dismiss()
                    checkNORM(norm.text.toString(), "ORANG")
                }
            }
        }

        return binding?.root
    }


    private fun checkNORM(norm: String, status: String) {
        progressLoading.show()
        progressLoading.setContentView(R.layout.dialog_loading)
        pendaftaranViewModel.checkNORM(norm)
        pendaftaranViewModel.pasien.observe(viewLifecycleOwner, Observer {
            if(it !== null) {
                if(status == "SENDIRI") {
//                    userManager.setNorm(it.data.NORM)
                    if(sharedUsers.norm.isNullOrEmpty()) {
                        sharedUsers.norm = it.NORM
                    }
                    val intent = Intent(requireContext(), Pendaftaran::class.java)
                    intent.putExtra("ID", it.ID)
                    intent.putExtra("NORM", it.NORM)
                    intent.putExtra("NAMA", it.NAMA)
                    intent.putExtra("ALAMAT", it.ALAMAT)
                    intent.putExtra("TANGGAL_LAHIR",it.TANGGAL_LAHIR)
                    intent.putExtra("JENIS_KELAMIN", it.JENIS_KELAMIN)
                    intent.putExtra("STATUS", it.STATUS)
                    intent.putExtra("PENDAFTAR", status)
                    progressLoading.show()
                    startActivity(intent)
                    activity?.finish()
                }
                if(status === "ORANG") {
                    val intent = Intent(requireContext(), Pendaftaran::class.java)
                    intent.putExtra("ID", it.ID)
                    intent.putExtra("NORM", it.NORM)
                    intent.putExtra("NAMA", it.NAMA)
                    intent.putExtra("ALAMAT", it.ALAMAT)
                    intent.putExtra("TANGGAL_LAHIR",it.TANGGAL_LAHIR)
                    intent.putExtra("JENIS_KELAMIN", it.JENIS_KELAMIN)
                    intent.putExtra("STATUS", it.STATUS)
                    intent.putExtra("PENDAFTAR", status)
                    progressLoading.show()
                    startActivity(intent)
                    activity?.finish()
                }
            }

            if(it === null) {
                progressLoading.dismiss()
                Toast.makeText(requireContext(), "Nomor Rekam Medik tidak ditemukan!", Toast.LENGTH_SHORT).show()
                findNavController().navigate(R.id.action_pendaftaranDialogFragment_to_homeFragment)
            }

        })
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }


}