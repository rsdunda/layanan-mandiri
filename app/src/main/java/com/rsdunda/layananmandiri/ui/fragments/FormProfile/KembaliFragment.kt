package com.rsdunda.layananmandiri.ui.fragments.FormProfile

import android.content.DialogInterface
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.rsdunda.layananmandiri.R
import com.rsdunda.layananmandiri.databinding.FragmentKembaliBinding


class KembaliFragment : BottomSheetDialogFragment() {
    private var _binding: FragmentKembaliBinding? = null
    private val binding get() = _binding

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        _binding = FragmentKembaliBinding.inflate(inflater, container, false)

        binding?.btnTetap?.setOnClickListener {
            findNavController().navigate(R.id.action_kembaliFragment_to_bpjsFragment)
        }


        binding?.btnKembali?.setOnClickListener {
            findNavController().navigate(R.id.action_kembaliFragment_to_bioFragment)
        }

        return  binding?.root
    }


    override fun onCancel(dialog: DialogInterface) {
        super.onCancel(dialog)
        findNavController().navigate(R.id.action_kembaliFragment_to_bpjsFragment)
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }

}