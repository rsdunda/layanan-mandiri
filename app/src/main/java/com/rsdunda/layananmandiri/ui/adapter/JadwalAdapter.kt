package com.rsdunda.layananmandiri.ui.adapter

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.lifecycle.Observer
import androidx.lifecycle.observe
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.card.MaterialCardView
import com.google.gson.Gson
import com.rsdunda.layananmandiri.R
import com.rsdunda.layananmandiri.data.model.jadwal.Hari
import com.rsdunda.layananmandiri.data.model.jadwal.Jadwal
import com.rsdunda.layananmandiri.ui.activity.JadwalHari
import com.rsdunda.layananmandiri.ui.viewmodel.JadwalViewModel

class JadwalAdapter(
    private var jadwal:MutableList<Jadwal>,
    private var context: Context,
    private var jadwalViewModel: JadwalViewModel): RecyclerView.Adapter<JadwalAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.poli_items, parent,false))
    }

    override fun getItemCount() = jadwal.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) = holder.bind(jadwal[position], context, jadwalViewModel)

    fun setData(data: List<Jadwal>){
        jadwal.clear()
        jadwal.addAll(data)
        notifyDataSetChanged()
    }

    class ViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {
        val btn_poli = itemView.findViewById<MaterialCardView>(R.id.btn_poli)
        val text_poli = itemView.findViewById<TextView>(R.id.text_nama_poli)
        val dataArr:MutableList<Hari> = mutableListOf()
        fun bind(data:Jadwal, context: Context, jadwalViewModel: JadwalViewModel){
            dataArr.clear()
            val sharedPreferences: SharedPreferences =
                context.getSharedPreferences("shared preferences", Context.MODE_PRIVATE)
            val editor = sharedPreferences.edit()
            val gson = Gson()

            text_poli.text = data.nama_poli

            btn_poli.setOnClickListener {
                data.hari.forEach {
                    dataArr.add(Hari(it._id, it.dokter, it.id_dokter, it.nama))
                }
                val json = gson.toJson(dataArr)
                editor.putString("jadwal", json);
                editor.apply()
                val intent = Intent(context, JadwalHari::class.java)
                intent.putExtra("nama_poli", data.nama_poli)
                context.startActivity(intent)
            }
        }
    }
}