package com.rsdunda.layananmandiri.util

import android.content.Context
import androidx.datastore.preferences.core.booleanPreferencesKey
import androidx.datastore.preferences.core.edit
import androidx.datastore.preferences.core.stringPreferencesKey
import androidx.datastore.preferences.createDataStore
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.launch

class UserManager(val context: Context) {
    private val dataStore = context.applicationContext.createDataStore(name = "users_pref")

    companion object {
        val NAMA_LENGKAP = stringPreferencesKey("NAMA_LENGKAP")
        val IS_AUTH = booleanPreferencesKey("IS_AUTH")
        val UID = stringPreferencesKey("UID")
        val NOMOR_HP = stringPreferencesKey("NOMOR_HP")
        val NORM = stringPreferencesKey("NORM")
        val NIK = stringPreferencesKey("NIK")
        val NO_BPJS = stringPreferencesKey("NO_BPJS")
        val KELAMIN = stringPreferencesKey("KELAMIN")
        val FOTO = stringPreferencesKey("FOTO")
        val ALAMAT = stringPreferencesKey("ALAMAT")
        val ROLE = stringPreferencesKey("ROLE")
        val SESSION = stringPreferencesKey("SESSION")
    }

    suspend fun setSession(uid: String) {
        dataStore.edit {
            it[SESSION] = uid
        }
    }

     fun setNorm(rm: String) {
         CoroutineScope(Dispatchers.Main).launch {
             dataStore.edit {
                 it[NORM] = rm
             }
         }
    }

    suspend fun clearStore() {
        dataStore.edit {
            it[NAMA_LENGKAP] = ""
            it[NORM] = ""
            it[NO_BPJS] = ""
            it[NIK] = ""
            it[KELAMIN] = ""
            it[ALAMAT] = ""
            it[NOMOR_HP] = ""
            it[FOTO] = ""
            it[ROLE] = ""
            it[IS_AUTH] = false
            it[SESSION] = ""
        }
    }

    suspend fun storeUsers(
        nik:String,
        norm:String,
        nobpjs:String,
        nama: String,
        kelamin: String,
        alamat: String,
        no_hp: String,
        foto: String,
        role:String,
        auth: Boolean,
        session: String
    ) {
        dataStore.edit {
            it[NAMA_LENGKAP] = nama
            it[NORM] = norm
            it[NO_BPJS] = nobpjs
            it[NIK] = nik
            it[KELAMIN] = kelamin
            it[ALAMAT] = alamat
            it[NOMOR_HP] = no_hp
            it[FOTO] = foto
            it[ROLE] = role
            it[IS_AUTH] = auth
            it[SESSION] = session
        }
    }

    val nama_lengkap: Flow<String> = dataStore.data.map {
            it[NAMA_LENGKAP] ?: ""
    }

    val norm: Flow<String> = dataStore.data.map {
        it[NORM] ?: ""
    }

    val nik: Flow<String> = dataStore.data.map {
        it[NIK] ?: ""
    }

    val no_bpjs: Flow<String> = dataStore.data.map {
        it[NO_BPJS] ?:""
    }

    val kelamin: Flow<String> = dataStore.data.map {
        it[KELAMIN] ?:""
    }

    val alamat: Flow<String> = dataStore.data.map {
        it[ALAMAT] ?:""
    }

    val nomor_hp:Flow<String> = dataStore.data.map {
        it[NOMOR_HP] ?:""
    }

    val foto:Flow<String> = dataStore.data.map {
        it[FOTO] ?:""
    }
    val role:Flow<String> = dataStore.data.map {
        it[ROLE] ?:""
    }

    val isAuth:Flow<Boolean> = dataStore.data.map {
        it[IS_AUTH] ?: false
    }

    val session:Flow<String> = dataStore.data.map {
        it[SESSION] ?:""
    }

}
