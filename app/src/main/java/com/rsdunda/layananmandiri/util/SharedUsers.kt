package com.rsdunda.layananmandiri.util

import android.content.Context
import android.preference.PreferenceManager

class SharedUsers(val context: Context) {
    companion object {
        private const val NAMA_LENGKAP = "NAMA_LENGKAP"
        private const val IS_AUTH = "IS_AUTH"
        private const val UID = "UID"
        private const val NOMOR_HP = "NOMOR_HP"
        private const val NORM = "NORM"
        private const val NIK = "NIK"
        private const val NO_BPJS = "NO_BPJS"
        private const val KELAMIN = "KELAMIN"
        private const val FOTO = "FOTO"
        private const val ALAMAT = "ALAMAT"
        private const val ROLE = "ROLE"
        private const val SESSION = "SESSION"
    }

    private val data = PreferenceManager.getDefaultSharedPreferences(context)

    var uid = data.getString(UID, "")
        set(value) = data.edit().putString(UID, value).apply()

    var nomorhp = data.getString(NOMOR_HP, "")
        set(value) = data.edit().putString(NOMOR_HP, value).apply()

    var norm = data.getString(NORM, "")
        set(value) = data.edit().putString(NORM, value).apply()

    var no_bpjs = data.getString(NO_BPJS, "")
        set(value) = data.edit().putString(NO_BPJS, value).apply()

    var nik = data.getString(NIK, "")
        set(value) = data.edit().putString(NIK, value).apply()

    var nama_lengkap = data.getString(NAMA_LENGKAP, "")
        set(value) = data.edit().putString(NAMA_LENGKAP, value).apply()

    var kelamin = data.getString(KELAMIN, "")
        set(value) = data.edit().putString(KELAMIN, value).apply()

    var foto = data.getString(FOTO, "")
        set(value) = data.edit().putString(FOTO, value).apply()

    var alamat = data.getString(ALAMAT, "")
        set(value) = data.edit().putString(ALAMAT, value).apply()

    var isAuth = data.getBoolean(IS_AUTH, false)
        set(value) = data.edit().putBoolean(IS_AUTH, value).apply()
    var role = data.getString(ROLE, "")
        set(value) = data.edit().putString(ROLE, value).apply()

    var session = data.getString(SESSION, "")
        set(value) = data.edit().putString(SESSION, value).apply()
}